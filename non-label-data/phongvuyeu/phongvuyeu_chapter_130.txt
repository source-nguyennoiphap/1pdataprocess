








    和恶魔合作，显然是与虎谋皮，不管布局如何，是否成功干掉卡文斯，最终我们肯定会翻脸，然后好好厮杀一场。

    但若是拒绝了合作，却很有些可惜，毕竟索福克里手中有卡文斯的准确情报，这可是现在我最需要的东西。

    我陷入了犹豫之中，下一霎，庞大的巨爪却猛地从天而降，提我做出了袭击。

    但可惜，被其击中的，却只是史莱姆变成的化身碎片，而出手的，却也只是虚影，索福克里的本体动都没动。

    从一开始，我们谈话的就不是本体。

    我默默的站在云端之中，看着下面依旧在喝茶的大恶魔经到了时候，这一击，只是一个讯号。

    有些话实际上不用说的太明白，不管是否答应入局，至少要展示出一定的战力来，才有说话算数的权利，这就是深渊大佬们的交流规则。

    既然很有“诚意”的先说出了自己的要求，现在，阿福正在试探我突破后的个人实力，看能否有平等的话语权。

    但若我表现的太弱或露出破绽，他也会很开心的顺手除掉我，当然，反过来也是一样，若有除掉眼前的大恶魔的机会，我又怎么会错过。

    其实，既然他今天堵到了我，这一场就怎么都免不了的。

    “呵，不出手试试的底气，你都不会甘心？”

    虽然从生死互搏变成了相互试探，危险性却没有降低半分，一旦露出破绽或实力差距明显，就会立马重新变成生死搏杀。

    圣洁的光辉在我身边汇聚，我深吸一口气，代表黎明的圣剑双手紧握，背后的天使羽翼全面展开。

    对于天使来说，羽翼，不仅仅是装饰和飞翔的功能性器官，它真正的作用是存储、汲取更多的秩序之力。从某种意义上来说，羽翼的数量和质量也代表了那个天使的秩序之力的上限，越是强大的天使，羽翼看起来越奇怪。

    两翼、四翼、六翼、十二翼、金属翼、光翼、炙翼、龙骑兵、浮游炮.貌似混进来什么乱七八糟的。但天使的羽翼根据他们的阶级和职位，五花八门各式各样，从某种意义上来说，这的确是一种功能性器官。

    它们最基本的能力，就是从上层位面抽取秩序之力。补充自己的消耗和增强战力，所以它们的翅膀总是微微光，而且极度擅长持久战。

    我深吸了一口气，无尽的圣光被从上层位面汲取而来，整个世界被金色的光华洗礼，和过去相比，圣光的聚集度至少快了十倍。

    之前我的战争天使变身，依旧没有改变我的生命形态本质，而现在，我的血脉却能够给与我实实在在的力量了。

    战争天使。是我的秩序天使血脉的名称，在古老的位阶之树上，这并不是一种常规存在的古天使，而是一种被造物主赋予某些领域极其优先的古天使的荣誉头衔，而它的名字，说明了这类天使的特征。

    无他，善战。

    而作为秩序阵营的最顶端的战士，本身最大的依仗就是对秩序之力的使用，还是什么，比无尽的秩序之力。更能增幅他们的战力。

    从某种意义上来说，这双隐隐约约散着金光的羽翼，也是一件专门用来汲取、增幅秩序之力的顶级史诗武器。

    我甚至可以隐隐约约感觉到，汲取的圣光之力的上层位面出口。就是天之柱（秩序之柱）的核心处，和冥河对等的天之柱，是一切秩序之力的根源，却也是灵魂外的存在无非接触到的光之河。

    能够从光之柱中汲取最纯粹的秩序之力，看来，秩序女神再次作弊了。

    整个天空被圣光渲染。我的羽翼已经化作了足以媲美战舰长度的宏伟光翼，但我却没有感觉到自己已经抵达极限。

    在某位造物神的权限之下，或许我汲取的圣光的数量不及纯粹的秩序之神，但在质量至上，恐怕还要领先。

    在海量的圣光之力的支持之下，圣剑黎明的光之剑刃被无限延展，这样的设计，也是我用来斩杀大型恶魔、亡灵的依仗。

    “第二模式”

    剑柄上的魔纹被点燃，五个小配件却从中飞离开了。

    一个高飞上高空，另外四个，却向四周弹开，占据了四个角的位置。

    而持有圣剑的我，本身却肆意散着圣光，成为名副其实的光源。

    无尽的圣光在期间来弹射，每一次弹射，都在增加己身，这是光镜制造的特殊领域。

    圣光在辅助魔镜的增幅反弹之下，变得更加炙热和要命，同时兼具净化和修正的纯粹圣光之力，在净化修正这个领域。

    下一霎，随着光之链接的建立，我就不再是唯一的光源了，头上、四周，五个光源同时被点亮，整个世界被圣光洗礼。

    即使强大如索福克里，在这前所未有的圣光领域的洗礼之下，也瞬间变得睁眼如盲。

    恶魔强壮的庞大身躯带来的更大的照射面积，持续性的纯净圣光会净化、消弱范围内的一切非秩序力量，或许比不上原教旨圣光那么霸道，但这种持续性的消弱，在面对强敌的时候反而效果更好。

    大恶魔特有的混沌扭曲之力，能够本能的将周遭的环境深渊化，变成挥自己力量的主场，而我的圣光领域，却能不用消耗的抹去这些混沌之痕。

    庞大的身躯反而成了弱点，空气中的高浓度圣光，正在如硫磺一般在溶解大恶魔的身躯。

    或许短时间内还威胁不到深渊主神，但这持续性的消弱，是没有止境的，若我现在手上有完整版的寒冰魔剑北地，两把魔剑配合，我就可以尝试一下将索福克里永久性的留在这里了。

    我并不想一上来就揭晓自己的底牌，更不想在没有必杀的把握面前丢出杀手锏，但当自己的对手是索福克里这样的存在的话，留手也就等于送死了。

    而这广域的光之散播，却只是开始。更多的圣光聚集在圣剑黎明之上，“光的集中”的概念，将其化作致命的光之针。

    等光之力聚集到极点的时候，那一光刃。将具备一击弑神的威力。

    光之剑已经延长到了数百米，我却依旧将其牢牢的握着手中，只是剑刃所指之处，已经瞄准了恶魔之主的核心。

    “聚集，聚集。更多的聚集，对于混沌主神级的存在来说，禁咒类的扩散伤害毫无意义，只有压缩到极致的纯净秩序之力，才有可能击穿他们的防御，毁灭他们的要害恶魔心脏。”

    “嗷！”

    大恶魔仰天出怒吼，扭曲的混沌之力如火山一般喷涌而出，黑雾熔渣覆盖在身躯之上，在抵御圣光的洗礼。

    背后的斩巨剑握在手上，却和血肉融合到一起。山一般的巨剑上笼罩了厚厚的血光，当大恶魔拿住自己真正的力量的时候，整个世界都仿若在恐惧般的颤抖。

    大恶魔猛地吸气，无尽的深渊之力从地表下涌出，他已经不惜代价完成了和深渊位面的链接，整片大地在颤抖中解体，一切都归于混沌。

    在漆黑大地上，混沌的主神越狰狞，庞大的身躯又胀大了三分，随之而来的巨剑。已经足以斩开任何山岳。

    终于，他动了。

    巨大的恶魔挥动比肉身更加庞大的巨剑，斩向了天空。

    大恶魔的踏足就在制造地震，红色的血光让巨剑猛地延展开来。挥动的血光甚至直接击穿了云朵。

    看到这一剑的时候，我就知道了，不管我从何种角度躲避，最终，这一剑还是会和我的光剑相撞。

    其痕迹，似乎隐隐约约有因果的味道。这位以狡猾闻名于世的大恶魔，居然还是位极其出色的大剑圣！

    而索福克里已经成功将这里强行和深渊链接，这一剑所牵动的，将是深渊的混沌之力。

    “该死，不是说试探吗？这么拼命干啥，我又弄不死你，最多让你深渊睡个几百年。”

    而此时的索福克里，心底也在骂娘，这不是试探吗，怎么一动手就丢核弹压底牌了，上手就大招互拼，这是哪门子的试探。

    其实他那里知道，刚刚突破的罗兰手上就这一招专门针对恶魔的王牌，就是为了上门的主神大佬准备的圣光核弹。

    双方都担心对手把试探变成了绝杀，不断体力的结果，就是一出手就变成了全力互拼。

    一出手丢大招，另外一方还留着余地，难道是等死？

    索福克里无比后悔自己的鲁莽，他的目标只是试探罗兰是否有威胁恶魔主神级存在的杀手锏，结果不仅真有，还用到了自己身上。

    早知道一动手就成拼命，谁傻得和二愣子同归于尽，但此时，谁先收手，等于把主动权交给对方，主动送死。

    “嗡嗡嗡！”

    当延展开来的纯白光剑和血红光芒相撞，整个世界都荡着蜂鸣声，而在平静了一霎之后，遮天的灰色光柱射向了天穹。

    那是无尽延伸的巨型光柱，就连早已经离开了数个小时的浮空艇上，也可以清晰的看见那直入天顶的光柱。

    而下一刻，光柱还在，去向更高处延展，更为致命的冲击波却开始扩散，它们，将会把这片荒原彻底变成无人区。

    秩序之力、混沌之力，不断的冲撞、爆炸，即使埋在地下的生物，也会被侵蚀洗礼。

    冲击波不断的延展开来，似乎永无止境。

    而即使在数十公里之外的浮空艇，也在冲击波中苟延残喘，变得更加残破可怜。

    他们应该庆幸，若此时的他们在地上，恐怕已经失去了性命。

    或许，过上几十年，这里依旧会寸草不生。

    和过去历代的圣战一般，历史上上会记载一笔，这片荒原，曾经作为主神级强者的交手战场，彻底毁灭。

    或许，过上几年，这里会出现诡异的新生物种，就如往昔的古战场一般。

    而在冲击波爆的同时，交战的双方，都已经消失的无影无踪。

    二十分钟之后，一个焦黑的身躯落在了浮空艇的甲板之上，他浑身已经被烧的漆黑一片，火红一片，混沌和秩序的交锋还在肉身上进行，扭曲的血色雾气还在侵蚀焦炭化的肉身，靠近他的甲板，要么直接腐化风化，要么就被点燃。

    周遭的守卫们在震惊的时候，也急匆匆的去拿水灭火，有人却现这刚刚落下身影怎么这么熟悉。

    “这不是刚刚离开的罗兰殿下吗？怎么熟了？”

    “不愧是不灭的罗兰，熟透了还活着。哇塞，他还能瞪人。”(未完待续。)

    ps：那啥，看来已经友看出来了，嗯，疯巫妖已经接近尾声具体啥时候完结咱也不知道，故事讲完了就该完了吧，为了不烂尾，咱会努力的。

    ◆地一下云来.阁即可获得观.◆







  



