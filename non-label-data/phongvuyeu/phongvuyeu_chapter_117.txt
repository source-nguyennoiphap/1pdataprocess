








    拜尔王都，历史悠久的古老都城。一

    从城市上空往下看，和**年前那次起，城市的规模比过去更加辽阔了数倍。

    不，准确的说，应该是是增加了几个新型的功能区，增加了数个新兴的卫星城。

    最显眼的，大概就是北面的辽阔区域，那里，本来是一片群山。

    周遭的山崖被挖空，尺寸明显长的巨大的宫殿群在其上林立，经常可以看见巨龙在其中起落，那是才开始运作数年的拜尔龙城。

    而在龙城之下，是一座白石砖瓦的新竞技场，骑士们在彼此角斗磨砺武技，也是在给巨龙们展示自己的武力。

    在拜尔皇帝和诸位龙族大佬的共同努力之下，也是在外界的压力之下，龙族们逐渐放弃了过往的傲慢，和骑士一起作战并不是那么难以接受。

    而现在，已经不是非要高阶骑士、高阶巨龙的配合，才给人无解的感觉，海量新式的装备的介入，让不少年轻龙都选择搭上一个随着新装备附赠的骑士.这话怎么这么别扭，但事实就是如此，不少骑士在巨龙眼中就是装备附赠的操作手。

    尤其是三年前，新型魔导火炮毒蝎2、中型能量池的成功问世，带着数门重型魔晶炮的巨龙，已经越来越有新生天空霸主的势头。

    这种巨龙，已经有了新的名称，甚至有了常规兵种化的可能魔甲战龙。

    城西边的新城，却是技师、工程师、施法者的新区域。

    明明作为有这龙骑士传统的级大国，在现魔导技术的重要性之后，却毫不犹豫的引进开。

    海量的资源、财富、人口储备，最重要的是足够的受教育人口基数带来的魔导人才储备，让其迅后来居上，在龙骑皇帝海伦特的率领下，越来越有中兴的态势。

    其实很好理解，砸钱向盟国买技术、买人才，然后丢出政令让学者改行、投资有前途的研究者。就算不能立刻砸出成果来，也可以让研究者渴望这样的研究环境，主动前来，再让新兴的人才出于利益考虑。选择这个专业，再过上十年、二十年，就等着收获成果了。

    但和同样是新兴魔导大国的奥兰不同，有足够的巨龙作为战争资源储备，他们并没有把精力和资金投入重船、战车的建设之中。反而着重各种小型、重型实战装备的研制。

    实战，是他们选择研究领域的唯一理由。

    魔导引擎、微型（中型）能量池、实战性火炮/重弩、魔导护甲，这些被其他国家视作细微末节的重点技术，他们都将其视作重点领域进行研究。

    刚才提到了魔甲战龙，带上重型/中型能量池作为第二魔力库，浑身装备了数百块魔导护甲，带着四至二十门新型火炮，武装到了牙齿，同阶一打五不要太容易。

    这种秘密武器，在数个月前和亡灵军团的战争中突然登场。取得了惊人的战绩，吸引了无数的注意。

    世人这才现，原本以为古板缓慢的拜尔，实际上在新生的魔导领域已经走到了非常远。

    魔导战龙，在拜尔今年新年表的皇帝演讲之中，是一种已经可以规模生产的常规兵种，现在整个拜尔，有三百一十二骑。

    防护力、持久作战能力、攻击能力，都在魔导技术的增幅下被大肆加强。

    魔导防护让普通巫妖的术法攻击无效，强悍的肉身加上碾压性的火炮伤害。魔导战龙在天灾的战场上肆意杀戮。

    实战证明了这种组合的强大和可怕。

    这个兵种，在小妖精的评价之中，是传奇级，也就是说全员传奇以上的平均战力的可怕军团。从纸面上，已经是已经是全世界最强的战团了。

    当然，也不是没有坏处的，海量的资源、金币消耗就不说了，据说为了这些宝贝疙瘩，海伦特陛下已经穷的开始拍卖随身物品吃咸菜了。

    另外一方面。披甲的大部分都是青年龙、壮年龙，古龙、远古龙真正的杀手锏，还是他们的自身，在高端战力上，魔导技术还帮不上忙。

    而且，由于太多的重型装备披在身上，即使有浮空引擎的帮助，这种战龙的敏捷度也是不值得期待，若关掉引擎，或是浮空引擎出现了故障.他们会掉下来吧。

    “不会操作手册上有，遇到这种情况，直接解体丢下所有装备就是。海伦特陛下说过‘装备没了可以再造，龙和人死了就没了’。”

    刚刚进入拜尔的领空，就有两位最新锐的魔导战龙跟上来了，而还带来了随行的外交人员，显然是早就收到了风声。

    这位名叫斯蒂尔.周的中年外交官，以前去过岚国，和我打过照面，能派他来接应我，海伦特也算有心了。

    而这种出现故障，就丢掉装备人员优先的做法，无疑却从一个侧面上证明了拜尔的财大气粗。

    那一套龙甲的造价，给岚盟的话，都够造两艘驱逐舰了。

    或许，这就是大国和小国的国力区别，同样的技术在小国身上，只能短时间的领先赚点差价，甚至有的先进技术都造不起、造不出成品，而大国在列装之后，才会想着卖点旧货和边角料。

    前进号客船的引擎还没修好，我们没有在云间翱翔的资本，浮空艇飞翔的高度可以很清晰的看到地面，而两侧各一只的巨龙和上面的年期骑士，还时不时有好奇的眼光打量着我们。

    “似乎，这些龙骑士不是龙裔？”

    我随口一问，斯蒂尔却冒出了冷汗，从某种意义上来说，这应该是刺探军情了吧。

    他在犹豫，不知道自己是不是能够答，但有人却可以轻松的做出抉择。

    “嗯，圣战打到这个份上，那些老古董也看清了形势，在考虑龙族的自尊前，也想办法维护种族的延续和地位才是最要紧的。”

    随着厚重嗓音而来的，是一个披着厚重蓝色斗篷，穿着重甲带着王冠的中年男子。

    他走过来，直接给了我一个过度热情的拥抱，周围却跪了一地。

    “好久不见，罗兰殿下，欢迎你再度来到拜尔，期望这次您给我带来了好消息。”

    海伦特.米兰，过去的龙骑大公，如今的龙骑皇帝。

    和九年前相比，他的个头又长高了一了一点，刻意蓄下胡子和妆容大概让其显得成熟点，但从我眼睛来看，他的**年龄似乎反而变得更加年轻。

    可怕的力量蕴含在这具**之中，闭上眼，我的感官中这里只有一只强大的巨龙今的海伦特，像龙多过想人。

    这也是预期之中的，毕竟龙族的秘法中不少都是提升血脉浓度的，海伦特没有理由拒绝力量。

    而和过去那个腼腆的少年相比较，如今的海伦特，表情却有些夸张。

    “喔，不，您已经给我带来了好消息，一周前，您作为人类的一员，击败了混沌的主神，成就了不朽的荣誉。来，让我们为人类的英雄欢庆这一伟大的胜利。”

    自说自话完毕，拜尔的皇帝陛下兴奋的带头鼓掌，周围的随从和平民自然也出声音。

    一时之间掌声雷动，欢呼不断，就差点鼓乐手了。

    无事献殷勤，非奸即盗，这突然而来的过度热情，只让我觉得迥异和鸡皮疙瘩直起。

    “宣传，宣传。”

    低声的耳语，接着拉着我的手，向船下的民众挥手会意。

    我这才现，前进号已经进入了港口，而欢迎的人群却挤满了下面，隐隐约约居然过了前人，离得不远，还有被小龙提着的红字标语。

    “热烈庆祝岚盟和拜尔帝国结交八周年。”

    “欢迎岚盟领导人罗兰殿下莅临指导工作。”

    “热烈庆祝罗兰殿下除魔归来。”

    欢迎的人群、花朵、礼炮、军乐队都全了，唯一不像样的，大概是前进号已经快残废的模样，但有四条魔导战龙的护送，反而显得更加霸气从容，像是从战场返的勇士。

    我被海伦特扯着挥手，下面的欢呼声却猛地爆，甚至冲入云霄。

    让人在意的是，这适合并不是有意的安排，那些欢呼声太过真实。

    在这一霎，诸多想法在脑海中一晃而过，我的面容僵住了，然后瞬间，我就意识到了情况不对，立马转换成圣骑士标准笑容，向下面挥手。

    然后.我就被欢迎的人群埋住了。

    在一片混乱之中，我隐隐约约听到一些让人在意的词，比如说“援军前线”之类的，然后我就越心中有底了。

    三十分钟后，我们总算从热情的人群中钻了出来，还没来得及喝口茶水，我先问了。

    “海伦特，居然要我的到来作为政治宣传鼓舞士气？拜尔的形势差到这种地步了吗？

    闻言，海伦特僵硬的微笑转换成苦笑。

    他皱着眉，轻轻的按摩眉心，适合打算用此放松情绪。

    “果然，瞒不过你，是的，形势非常不妙。前线出了大乱子，我们打输了，输的很惨。认真了的亡灵大帝，们比预期的还要难对付十倍！”(未完待续。)

    ◆.◆







  



