








    “卡文斯是一个bug。”

    这句话并不是夸赞或贬低，而仅仅是陈述名为卡文斯的存在，是一个“世界”的错误。

    就我对秩序法则的理解，构建世界的四元素之中，只有时间和空间是最属于秩序侧的力量，他们是世界的轴、y轴，构成了整个世界的坐标系。

    而这两种力量，也始终站在秩序规则的最下端。

    是的，最下端，他们是构成一切的基石，诸多规则存在，是以时间和空间相对稳定为基础，时空之力，即是基石，自然也是顶端和根源。

    而混沌侧相对应的两大元素，就是灵魂和生命…….暂时就不展开了。

    时间和空间加在一起，就是时空，而时空之力最了不得的应用，却并不是高大上的时间停止，也不是神秘莫测的空间瞬移，而是诡异神秘到让人无法理解的“真实预言”。

    和普通的预言不同，那并不是投过命运长河的操控，也不是预言者的胡言乱语，而是运用时空之力，将所有的现实情况做成数据模型，然后进行推演，延长“轴”、“y轴”，得到新坐标，得到未来的模样。

    一个更为简单理解的例子，掌握这种能力的存在，看世界就像是看一部视频动画，她只需要拉动进度条，就让数据解码成画面，直接看到之后的剧情。

    这种能力，显然超越了凡人的理解范畴，就像是二次元无法理解三次元一般，“视频中的人”怎么可能获得拉动进度条的权限，这种gm权限般的能力，它也只握着这个世界的gm手中。

    阿斯忒瑞亚，秩序女神，是唯一掌握了这个权限的存在。

    而这个权限，显然有一个弱点。

    当这位女士，觉得视频的剧情不尽如意，她亲自动手修改剧情发展的时候，也就等于她放弃了“三次元存在”的身份，投入到了平面二次元之中，那么，她也很自然的失去了继续拉动进度条的权限。

    而她存在的本身，她所作出的一切行为，就已经改变了剧情的发展，让一切失去了控制。

    她只有一次修正的机会，自然要抓住关键点。

    “卡文斯是一个bug，这个世界原本应该没有卡文斯的。”

    深渊王子，命运之子，是辛西娅最宝贵的私生子，是深渊之力的结晶，是她获得圣战的关键角色。

    在原本的剧情……在阿斯忒瑞亚所看到的“视频”之中，名为罗兰的深渊王子，获得了异界的知识，成为了世界的“主角”，命运坎坷到可以写两千万字的小说…….省掉过程，被圣堂教会所代表的人类社会伤透了心的他，最终对人类失去了最后的信心，绝望步入了深渊之门。

    而当他率领恶魔大军返回人间的时候，也是人类彻底失去主宰权的时候。

    这样一个关键角色，就是改变一切的机会，阿斯忒瑞亚毫不犹豫的故技重施，残魂附在怀孕的罗兰母亲身上，静静的等待罗兰的诞生之时。

    但可惜的是，上一次的深渊王子的“背叛”导致了圣光和圣堂教会的诞生，吃过一次亏的辛西娅，也将深渊意志附在了罗兰母亲身上，准备亲自教导深渊王子的成长，亲自打造他的世界观。

    两个贼把手伸进了一个口袋…….咳，我是说两姐妹的喜相逢，脑浆都打出来了。

    最终，她们的争执，就如童话故事中两个母亲争夺一个孩子的所有权，各扯着一个手臂使劲拉扯，结果那个真正的母亲听到孩子喊痛无奈放手…….但那个明辨是非的青天大老爷，绝对没有想到一种可能性，两个妈都不是亲身的怎么办！

    “悲剧啊（易老师腔）！”

    可怜的孩子，还没出生，就被两后妈直接撕成了两半。

    在母体还在孕期的前提下，一个灵魂硬生生的变成了两个，肉体同时发生了变化，独生子变成了双胞胎，但灵魂不足是最残酷的先天不足，“失魂落魄”的产物不是白痴就是智障，若不想获得两个毫无用处的智障，两位“后妈”必须做点什么。

    她们能够做什么？作为两个灵魂体。

    是的，就如现在所看到的，两姐妹做出了同样的选择，她们用自己的灵魂补足了残缺的灵魂，就如母亲用乳汁喂养新生儿一般，但她们做的更过，她们相当于用自己的血肉填补新生儿的不足。

    是两个“母亲”和新生儿链接到了一起，成为了他们的一部分。

    而两个新生儿，也很“幸运的”，拥有了两位女神的部分特性，我会偏向于秩序，他会偏向于混沌，并不是偶然，而是必然。

    结果，就形成了一个很有趣的假设，类似那个很有名的，不知道换了多少次零件的木船，当把一个人的“零件”逐个替换，做出两个“灵魂缝合怪”，最终拼凑出的两个人，还是独立的个体吗。

    这个答案，其实本来应该无人知晓的，也没有实际意义，但有些意外，却提前给出了答案。

    我死了。

    深渊王子也是生物，也是轮回的一份子，它并没有超过轮回的特权，但我却在轮回中打了转，在冥河中游了趟泳，就重新活了过来。

    原因？并不是单纯的因为阿斯忒瑞亚的存在，她就是能够在冥河中保住自己，也保不住灵魂中身为凡人的一部分，作为一个残魂，她能动的只有嘴了。

    “冥河无法吞噬我，是因为…….我还活着啊。”

    是的，这个世界依旧把卡文斯和罗兰当做一个人存在，坠入冥河的只是“罗兰”的一部分，他还没有死亡，生灵还没有转换成死灵，灵魂依旧完整无缺，冥河水无法渗入，自然无法回归轮回。

    这个bug产生的原因？恐怕和两位女神的gm权限有着直接的关系。

    这就是名为“不灭的罗兰”的存在，那匪夷所思的不死性的真相。

    【卡文斯有没有死亡过？这恐怕只有他自己知道，但他既然知道不死性的真相，应该也是死过的，一个不惧怕死亡和失败的深渊王子，也难怪他成长的这么快了。】

    阿斯忒瑞亚比谁都明白，但一直没有说实话，相反，还主动引导我往穿越者灵魂特殊那方面想，其实仔细推敲一下，穿越者在本土还不是要去阴曹地府，有什么区别。

    而她一直不说实话的原因…….

    “你是怕我一旦情况不妙，就会选择采取类似同归于尽的手段来阻止卡文斯？”

    【你不会吗？】

    想了想，我异常老实的做出了回答。

    “不，我会。只要能阻止卡文斯打开晶壁之门，我会利用一切资源，采取一切手段。既然我们之间有这种联系，我自然会运用。”

    阿斯忒瑞亚看穿了我，说了谎，卡文斯也看穿了我，说了实话，所以他成功说服伊丽莎来阻止我，因为伊丽莎也看穿了我。

    “这还真是…….微妙的让人很不爽啊。”

    而此时，看着“妹妹”，感觉到那熟悉的灵魂波动，我很有些哭笑不得。

    万幸，眼前情况，也在预期之中，卡文斯并不是不可战胜。

    或许，我可以不用那那一招，就能够拦住卡文斯。

    “有一把魔剑，可以毁灭一切灵魂……”(未完待续。)







  



