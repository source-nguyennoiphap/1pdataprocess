








    当圣堂教会分部被驱逐后，迪芬德的圣骑士就变成了稀有职业，而今天，却有两个明显高阶的圣骑士在迪芬德最繁荣的商业区边走边聊。

    “哦，原来你是来这里执行公务的，战争刚刚结束就派你们来，看来教会本部也很重视这边的战局吗？”

    女骑士点了点头，这点不用否认，使节团的到来已经说明了一切。

    “在奥兰帝国都陷入了僵局的时候，谁都没有期望北方居然首先获得了胜利，这简直让各国为之震惊，这是这场圣战之中的开门红，再加上东岚在这个时候送上的重礼，参议会和冕下都非常高兴，才派遣我们进行嘉奖。”

    “嘉奖吗？那位罗兰可不好对付，能够让其满意的嘉奖很难拿出来吧。再说了，既然东岚送上了圣遗物，那么，返还的可不可能是几句口头上的感谢。难道高层的大人物舍得大出血么？”

    “…….虽然现在还算是机密，但既然迟早要公布，告诉你也无妨。这次卡洛玛一世给予的，恐怕是让世人不敢置信的殊荣……”

    “红翼之鹫”艾薇拉很是感激的眼前的年轻圣骑士，若不是他及时出现，替自己做了赔偿，恐怕这次丢人丢到家了，若是因为无法赔偿平民而被追到使节团去要债，那就真是颜面扫地了。

    而正当她处于困扰之中的时候，这个年轻的圣骑士出来了，虽然不知道这个北方的圣骑士是如何在这满是敌意的地域获得尊重的，但从那迅速服软的对方和周围人的态度来看，他似乎很受欢迎。

    而从一路上路人对其主动行礼的频率来看，在对于圣光职业明显有些成见的迪芬德来说，这简直不可思议。

    “或许，他可以对眼下的困境帮点忙。”

    艾薇拉已经打定主意把这位叫罗罗的圣骑士介绍给使节团了，地头蛇能够提供的情报和人脉，正是现在的使节团所需要的。

    而此时。罗罗问起了使节团来访的任务，考虑到对方明显也是圣骑士的一员，再加上被帮助的好感，艾薇拉也就随口说起了自己一行的目的。

    “也不是什么了不起的机密。但就是其中代表的政治意味很重要，有些敏感，请暂时不要外传吧，尤其不能让东岚王室知道。”

    “当然，我以圣光之名担保，绝对不会泄露给任何一个东岚人。“

    看到眼前的罗罗圣骑士以圣光之名担保，艾薇拉更添好感。

    “这次教皇冕下给予的，是一个圣徒的头衔！给予的对象，是那个有‘光耀之子’美誉的罗兰王子。”

    “？怎么可能？”

    “是啊，完全无法理解。我都不知道参议会和教皇冕下是怎么想的，圣（st）头衔能够随便给予吗？还是给一个活着的异邦人，或许他过去的确曾经是圣骑士的典范，或许寻回圣遗物的确是值得嘉奖的功绩，或许他也获得过封号的提案。但过去的都只是过去。看看这座城市吧，到处都是肮脏的亡灵，他和亡灵为伍，怎么可以把这么尊贵的封号和圣徒的殊荣赐予一个邪恶之徒。这简直是整个圣堂教会的耻辱。”

    边说，女骑士还气愤的直跺脚，这些日子积郁的不满已经溢于言表，在两位长者面前。她也只敢发发闷气，此时，遇到了宣泄的机会，抱怨连连。

    但也难怪她如此气恼，圣（st）的头衔，一般是给予圣堂教会承认的圣徒的。也基本等于死后才会加封的头衔，而这却不仅仅只是一个头衔，这代表了光明之神和圣堂教会对其的认可，即使荣耀，也是实质性的身份认证。

    这个头衔也会被加在国家之前。代表着最虔诚的信仰和圣光之神的眷顾，各大超级大国挤破脑袋抢一个圣头衔，也只有圣安东里奥等几个古老的帝国凭着过往无数年的贡献弄到手了。

    “圣光之神到底在想什么，怎么会突然送上一份大礼。“

    对我来说，若是弄到这个头衔，好处也不少。

    比如说，为了圣堂教会的名声和永远纯洁的圣光，圣徒是不会犯错的，若一个圣徒若是做出了什么恶行，都会被当做没有发生，简直是最完美的避难符。

    圣徒的头衔是不能随便给予的，一个人立下再多的功绩，也不可能一辈子保持不能犯错。

    也正因此，这头衔也基本成了死者的专利了，历史上活着的圣徒不是没有，但大部分都是圣堂教会的狂信徒，也多半和圣光之神展露的圣迹有关。

    那一年，当迪芬德上空，那代表着绝望的圣光最后闪耀的时候，在教会之中也有了不少不同的声音，短短两年就从青铜阶的圣骑士到达了传奇，本身就是神迹。那绝望中的纯粹圣光更是被看成了一种圣迹，于是，的确有主教提出给予罗兰圣徒头衔，当然，最后却被否定了。

    现在各方面的情报汇总过来，我差不多也清楚发生了什么。

    大概就是刚上任不久的教皇冕下收到这份大礼龙颜大悦，就想嘉奖些什么，然后考虑到历史上的提议，就给予了圣徒的封号，但显然使节团看到这里的情况又有了自己的想法，就在犹豫之间徘徊。

    “不，一个活蹦乱跳的圣者有多麻烦他们应该考量的出来，仅仅只是因为这个原因的话，给予的太多了点，肯定还有什么没有收集到的情报，这小丫头不想是能够能够藏住心事的人，那么，说不准连她都不知道。”

    但继续试探一下，至少也没错，说不准还有什么收获。

    “……这可是教皇冕下的决议，还是慎言吧。虽然我也没有资格说些什么，但我也觉得那个人配不上圣徒的殊荣，他的心目中从来没有圣光之神，难道教廷的其他人就没有意见了吗？”

    自己的意见被眼前的年轻骑士承认，艾薇拉不住的点头。

    “是啊，完全无法理解，教会中的争议不少，本来我还是保留意见的。但到了这里，看到这些不服圣光教义的异教徒，还有那光天化日之下的亡灵，不把他定为异端都很匪夷所思。教皇冕下肯定是被人迷惑了。才会做出如此荒诞的决定。不过……”

    “不过？”

    “实际上也没什么了，只是个人的猜测而已吧。或许是期望从新在这里建立圣堂教会的分教会吧，若是能够通过授予圣徒的头衔缓和双方的关系，也是很有可能的。”

    我点了点头，这都是预期之中的，就算立下律法之神为国教，境内也会允许其他秩序之神的分教会存在。

    而虽然很强硬，但我也做到了接受新的圣堂分会的打算，这不仅是政治上的需要，也有很高的实战价值。

    虽然律法之力在很多方面和圣光之力都有所一拼。在惩治恶徒上还超越了圣光之力，但在治疗方面，依旧是圣光之力的专利，我们这边的圣光职业者太少，也是很不利的因素。

    事实上。也正是圣光之力在治愈方面得天独厚的优势，让其治愈世人伤痛的传奇获得了无尽的好感，也让各国很难拒绝境内的圣光教会分部和圣光职业者的传播。

    艾薇拉说的很有道理，但…….

    “还是不够，永夜时期的深仇大恨可不是那么好化解的，授予圣徒身份可没有那么简单，这等于把过去的怨仇全部清空。没有理由的善意是危险的。尤其是这善意来自过去的敌人。”

    我仔细琢磨，但依旧一无所获，而看艾薇拉满腹怨念的摸样，我知道再追问也毫无意义了。

    “……..看样子她是不知道了。”

    而在街道一旁，那树立起来的大帐篷之下，亡灵骑士们正在准备着节目。死亡皇冠马戏团的开幕演出即将准时上演。

    带着南瓜帽的死亡骑士正在彼此嘲笑，披着“扫把”装饰的骨龙和试图骑着“扫把”的巫妖正在较劲，不用化妆就可以直接上场的无头骑士适合打算用自己的脑袋来表扬马球，那身为剑圣的骷髅将军在重温自己的转盘绝技。

    “该死的亡灵！愿天堂圣火净化你们！”

    注视正在排练节目的亡灵骑士们，感觉到那强大的死亡之力。咬牙切齿的艾薇拉，低声诅咒的眼前漆黑的存在们。

    我可以从中女骑士的眼中看出毫无掩饰的刻骨仇恨，看来，她对亡灵的仇恨不仅仅只是因为自己的职业。

    正当我打算问些什么的，一个熟悉的身影，却出现在街角，他似乎正在寻找些什么。

    “埃斯特拉达大人！您是来找我的吗？”

    艾薇拉急匆匆的向着那个身影走过去的时候，却没有注意到背后那个打算介绍给使节团的年轻骑士，已经悄然消失在阴影之中了。

    而埃斯特拉达看到艾薇拉的第一反应，却是一愣，他似乎从女骑士身上看到熟人的气息，当即追问道。

    “你刚才和谁在一起？”

    “…….正好，埃斯特拉达大人，我给你介绍一个人，他也是本地的圣骑士，他帮了我的忙，还很熟悉这里，罗罗，这位是埃斯特拉达大人，罗罗？人了？”

    回过头，那个友善的年轻圣骑士已经消失的无影无踪。

    很快，而在得知那位热心助人的圣骑士的外貌特征之后，埃斯特拉达面容上铁青一片，半响才挤出了一句。

    “……回去再说。”

    在一旁的小巷中，看着急匆匆离去的圣骑士组合，我也松了一口气，人的名树的影，“最强圣骑士”给我的压力实在不小，被他逮到的话，实在有些麻烦。

    “喂喂，怎么不去见见自己的导师，这么多年没见了，见面就躲掉，似乎很有些不礼貌吧。不管怎么说，他当年也教过你吧，太薄情了吧。”

    “……..你丫的剑术启蒙还是我教的，也没见到你对我有那点礼貌。”

    想起当年在千军万马中横冲直闯，对着我冲锋，还大喊“我的错误由我我自己纠正”的圣光之手，我就觉得有些头痛，和那个年轻的艾薇拉不同，不管是智力和经历，或是战力和麾下的力量，可以说的上无懈可击的埃斯特拉达都非常不好对付。

    作为基础职业的圣骑士何其多。圣骑士的进阶职业更是无可计数，能够让其他的圣骑士心服口服的承若其“最强”之说，本身已经说明了很多。

    但对我来说，他超凡入圣的实力只是一部分。作为最资深的圣骑士之一，他的人脉和威望甚至超越了历代教皇，那才是最要命的。

    若是他来一句“邪恶的罗兰必须被讨伐”，恐怕七成以上的圣骑士和牧师都会视我为死敌，我的乐子就真大了。

    当年也正是因为他的努力，才让圣骑士和牧师阵营和上层主教们近乎翻脸，所谓的圣战也没有了应该作为主力的圣骑士大军，但在永夜之役的时候，他和他麾下的骑士们，可是最让我头痛的死敌。

    看着这个依旧年轻的身影。我都不知道该如何应对了。

    “噗，原来还是真的。”在我烦恼的时候，没心没肺的亚当却突然想起什么，偷笑起来。

    “你笑什么？吃多了。”这家伙每次都能让我很不爽。

    蹲着的亚当笑的喘不过气来，半响才挥了手。

    “不是。不是。只是我最近听海洛依丝说了你们师徒一脉历来被徒弟干掉的传统，本来我还是不信的，现在看来，有些事情简直还是不得不信。”

    “你看，当年海洛依丝教了你吧，被你坑了，艾米拉教了你吧。也被你坑了。这就是两个了。”

    别说，亚当还扳起了手指，有板有眼的开始计数。

    “你教了伊丽莎吧，最后她把你砍了吧，第三个，埃斯特拉达教了你。最后也是刀兵相见吧。四个了吧，你也真是厉害，简直都成了诅咒，居然还敢收徒弟。看我的小安妮多乖，我看蕾妮和葛丽娜也都不是省油的灯。你就不怕重蹈覆辙……你拔剑做什么？”

    【系统提示：你…..】

    “你也闭嘴！“

    【我还什么都没说啊】

    “难道有好话吗？不就是那些恶搞的头衔和损我吗，若不是的话，若我猜错来的话，你丫就继续说啊。”

    【……..】

    系统的沉默（默认）让我觉得自己更加悲惨，我默默的拔剑，想起了“历史“中也被安妮干掉的往事，想起亚当不知道的那个精灵徒弟的背后一刀，心中的郁闷已经找到了发泄的目标。

    “我只是想起了你漏算的一个…………当年还不是我教的你，你还不是砍了我！今天，我就要清理门户！”

    好一阵厮杀后……好吧，我承认我吃了点小亏，一不小心估量错了双方的实力差距，但亚当绝对也不好干，他低估了我现在的蛮力，俺也是30点力量的蛮子了！

    虽然那丫变成了熊猫眼，但那家伙居然好意思用半神级的实力碾压我一个新人菜鸟黄金，在我稍微多挨揍……我们达成一致后，总算把话题扯到正事上来。

    “小红和安妮来了，叫你来就是来接她们的。”

    作为地下世界的代表，安妮带着使节团过来了，随行的，还有终于睡醒了的小红，这么久不见，还真有点牵挂。

    “那你还把我打成熊猫眼。这怎么接人。”

    “我还不是一样，你丫就不能下手轻点。”

    “等下你要给新手下训话，稍微准备下。”

    “新手下？什么？”

    “我要安妮带来了一部分新训练完毕的城管骑士和律法骑士，城里的那群混蛋到了该收拾的时候了。培训就交给你了。”

    “你不早说！我这样子怎么树立威信。”

    看着我刚刚亲手打肿的双眸，我得意的笑了。

    “呵呵，要的就是这个效果。休想在美女面前耍帅！”







  



