








    灾难总是从天而降，不幸的消息总是接踵而至，而正义往往还没天降就被射了下来.咳咳，我是说当你听到一个坏消息的时候，更多的坏消息往往就在路上。

    在第一个目击报告传来之后，异界的入侵者的消息接踵而至，其中大部分都不在我提供的资料单子内，但少数被证实的存在，依旧强大的匪夷所思。

    战争，似乎又要降临这片多灾多难的土地了。

    每一天，都可以听到某次发生而来战争，虽然由于神祗们的主动参与，这些小规模战争最后的胜利者都是防御方，但谁都知道这情况不对了。

    在有心人眼中，现在这些入侵者都却只是前兆，毕竟按照战争的规则，率先进入陌生敌境的，只会是炮灰和侦查兵。

    即使这些炮灰强大到必须用神祗应对卡索山脉被大批虚空扭曲者攻击的结果，就是两位强大神祗出手，一次性将山峦夷为平地。

    不是谁都有莫名其妙的自信坚信自己百战百胜，只有战争疯子才会在情报不明的时候盲目出击赌国运。

    在正常情况下，能够一锤定音的大军和大佬们，只会在情报充足的时候，才会考虑步入战场。

    而我们，每次都可以感觉到外界的注视者越来越多，越来越强大，即使他们依旧没有选择进入这个晶壁系，但恐惧和绝望的情绪反而散播的越来越多。

    “蕾妮”现在“突发恶疾”，在“后宫”之中疗养，岚盟的核心执政官只剩下“葛丽娜”一人，而“我”所能够做到的，只是尽可能确保岚盟度过这次浩劫。

    入侵者出现的情况很是随意，似乎在侵略、攻击，更多的却是试探。

    我自己出手过几次，大概是连异界的入侵都嫌弃北地太过荒废，来的都不是什么很强的对手，极光骑士团和北斗舰队很容易就清理干净。

    但谁都知道，这只是风雨欲来的景象。

    “计划进行的怎么样了？”

    “不怎么样，抽奖中的人大部分不想走，认为还没有到这一步。”

    “先让那些志愿者上方舟吧。”

    而在另外一面，最后的“末日方舟方案”，也被强行启动。

    通往冥府势力的主位面春之大地的传送门，每日的人流量大概已经超过数十万人次，但依旧有些不够。

    若未来的战争形势不妙，只要斩断所有的传送门，冥府和它绑定的春之大地将彻底和艾希没有关系，一个新的晶壁系将隔绝一切的窥探者。

    人类、兽人、精灵都爱船上，艾希的种子也可以在新的土壤上发芽，保留文明和种族的最后的火种，但不到最后一步，谁都不想离开离开这自己的世界。

    并不是我对未来的战争没有什么信心，而是习惯性的未算胜先算败，至少，要留下一条退路。

    而艾耶他们也赞同了我的判断，从最近的情报来看，冥府侧、圣光侧、自然侧的神祗都先后有出手的记录了。

    而在另外一方面，似乎随着大门的敞开，元素潮汐的活跃程度再度飙升，我们的日子变得更加好过，但从另外一方面，或许这也是那些外来入侵者进入这个世界的必要条件，就如当初主神、强大神祗降临主位面的限制一般。

    “还要等多久。”

    “快了，快了。”

    死猫有些熬不住了，于是她就跳上窗台眯着眼假寐休息了，剩下的交给艾米拉换班。

    值得一提的，就是海洛伊丝被伊丽莎好好收拾一顿之后，居然保住了性命，而随着大门的敞开，伊丽莎也没有必要继续阻拦我，返回了我的身边。

    我当然很欢迎这样强大战力的回归，毕竟此时的伊丽莎依旧深不见底的强大，只不过海洛伊丝和艾米拉似乎信不过她，坚持必须有人守护我，开始了两班倒的轮班，

    现在的我，坐在葛丽娜的小板凳上处理政务，艾米拉在客厅中喝着红茶看书，顺便盯着穿着女仆装认真擦玻璃的此世最强存在反正时间不多了，随便她了，没人打的过她，她喜欢打扫卫生就让她去吧。

    而沉着脸的她似乎最近心情不是太好，也是由于她是不是就会爆爆气，释放威压聚集魔力，做出点什么引人注意的事情，又偏偏没有什么实质伤害。

    时不时就惹出点麻烦，海洛伊丝和艾米拉才是感觉疲累，必须换班。

    我知道她是找机会发气迁怒，而海洛伊丝和艾米拉又有些莫名的理亏，结果就是我们很有默契的让伊丽莎自己随意了。

    而我这个小院子的气压最近也越来越低，半神和主神莫名其妙的斗着气，神仙打架凡人遭殃，已经有人走着走着就莫名昏倒，据说侍从官和行政人员都已经开始互相推卸向我汇报工作的职责了。

    “.伊丽莎，冰河镇那里有一群不请自来的蚂蚁群，你去处理一下。”

    而这个时候，伊丽莎就很方便了，有她出手的话，至少北地已经成为了最安全的区域，而她的暂时离开，也让我们都松了一口气。

    “还有多久。”

    艾米拉默默的合上书本，没有来头的说上了一句。

    “哇哈哈，你说啥？哦哦哦，冰河镇也有点远，就是伊丽莎应该也有四五个小时才能搞定回来。”

    “.别装傻，你知道我问的什么。”

    我默默的叹了口气，这个问题，其实我自己都不知道该如何回答。

    “几个小时？明天？几天？几个月？说实话，我也不知道自己还能撑多久，但肯定比那个家伙久。”

    艾米拉默默的看着我，没有说话，但那微红的双瞳和静静的目光，却让我有些承受不住。

    “抱歉了，我也没法的。我知道那家伙难缠哪想到会那么难缠。当时有多危急你也知道，我只能拼了.”

    坚强的女孩，那默默留下的眼泪，堵住了我所有的解释。

    “抱歉了”

    最终，只能化作苍白无力的解释。

    “罗兰，你要死了。”

    死猫的话语让我无法辩驳，是啊，我要死了，或者，已经死了。

    我并不是有着异常的兴趣才继续待着葛丽娜的**之中，我的肉身和卡文斯已经同归于尽，我们的灵魂本体应该还泡在冥河水中，现在强行留在凡间的，只是强行降临的意志，而随着冥河的腐蚀，这份自我还能够坚持多久，就真的是个问号了。

    伊丽莎的判断没有错误，我和卡文斯的决斗，最终只会变成同归于尽的结果，她的背叛只是为了避免这份可能。

    而艾米拉和海洛伊丝对伊丽莎有些理亏，或许就是最后的结果，依旧让人无法接受。

    这次，当兄弟两人的灵魂同坠冥河，轮回终于抓住了我们这对偷渡犯，再也没有网开一面的理由了。

    我能够时刻感觉到冥河的召唤，也感觉到自己的灵魂本体越来越是虚弱，但至少现在.

    “海洛伊丝，能不能有点良心，怎么说我都是你的主人，也喂过你不少猫粮”

    “瞄，我当然记得，我是你魔宠，你死我死，不就是冥府审判和炼狱处刑吗，我们早就约好了，一起去就是了。嗯，终于能够好好休息了，我们应该开心才对。主人，拜你的魔宠契约所赐，就是转世我们也会在一起。”

    海洛伊丝你的话很让我感动，但能不能不要边说对着艾米拉得意的笑，你对魔宠契约有不满我们可以解除，这种明白的挑衅，学姐学姐已经笑的很危险了！

    “.我。”

    似乎一直在犹豫，最终，有些话艾米拉没有说出口。

    她是木灵一族的主神，梦境之森的本体，她一旦死亡，整个种族将为之陪葬，这样的承若，她无法做出。

    “对了，要不顺便写个遗嘱吧先提前销毁老宅的那些小薄本，还有那些珍藏品要不要交给亚当保管.还是算了吧，隔两天就被玛格丽特找出来一把火烧了，罗兰的遗物是小黄书之类的，传出去实在太可悲了。”

    我摇了摇头，考虑自己的身后事，提前处理掉黑历史，虽然别的男人估计这辈子都没有机会，但实在太过惊悚，还是就此打住吧。

    老神在在的悠游自在考虑有的没的，我手上的工作也接近尾声.实际上政务根本处理不完，能够尽心到这种地步，已经难得可贵了。

    我们现在的情况其实很不妙，整个世界的情况都不妙。

    “.简直，像是待宰的牛羊。”

    圣光之主出去了，然后被秒杀了，已经证明了出去是自寻死路，当若不出去，也就等于把主动权交给了对手。

    我知道现在都是瞎忙，只是用工作来打发焦虑的等待期，一边准备方舟，一边等待那终将抵达的最终入侵。

    “.再不快点，就等不到了。”

    我无奈的看着自己娇嫩的小手，原本红润的皮肤变的苍白冰冷，死灵在活人**戴的太久，给葛丽娜的伤害越来越大，而**潜意识对我的排斥，也越来越明显。

    “呵，我这状态，死灵附体肉身，应该也算巫妖了吧。”

    没有等我感叹世事无常，轮回无法估测的时候，窗户突然打开，一轮高悬空中的苍月，吸引了我们的注意力。

    此时的夜已深，但天空中却有两轮月亮。

    我们并不需要思考就能够区分那是真的月亮，因为艾希的月亮绝对不会长出跨越半个星球的裂齿大嘴，还有那邪恶至极，看整个世界就像是看一个美味奶酪蛋糕的贪婪眼神。

    而现在，他张开血盆大嘴，正在一口一口撕咬艾希世界的月亮。

    此时，月神的无声痛嚎在所有生灵耳间响起，但谁都无能与力。

    吞星魔神，那群强盗的首领未完待续。







  



