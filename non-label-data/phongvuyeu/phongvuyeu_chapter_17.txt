








    从某种意义上来说，满世界战乱也不是一点益处都没有的，至少，我们北迁的车队一路上顺畅了不少。

    和其他人、势力成为朋友的最快方式？出现大家的共同敌人就是了。

    往日，带着这么多的精灵进出他国的国境，还带着浮空战舰群护送，简直是给自己找不自在。

    就算别人不觉得你会是入侵的，那些精灵狩猎者和一路上的刁难也是无尽的麻烦，走走停停才是常态，一路通行只是会梦想。

    而现在，警告已经化作现实，深渊势力的袭来，让整个人类社会都有了莫名的危机感和紧张感，山不转水转，对和自己没有利益冲突的北方大国，尤其是军火出口大国，结个善缘显然比得罪的好。

    而阎王好说小鬼难缠的现象，也是往往是因为“阎王”不怎么管事，不在乎这些主动上门的访客，没有严加看管手下和制定纪律制度。

    现在听说岚盟的当家蕾妮女士路过，别说通行许可了，沿路各个领主主动上门迎宾的都不少，拉关系的，谈军火买卖的，吹吹打打不停歇，让精灵们好好见识了一下岚盟在人类社会的地位。

    当然，一个精灵就是巨额财富，在某些人眼中，车队是带着一座金山上路，就算用军舰骑士护送，觉得能捞一笔暴富的也不在少数，我们外围总是跟着乱七八糟的人，黑衣人白天就飞来飞去，那些盗贼、探子可以在屋梁上、影子中打麻将了。

    无奈之下，黑龙、红龙被派出去充门面，不时表演一下龙吼和对空喷火，别说。对这些闲散人士，这些传说中的巨兽比钢铁战舰还要有“说服力”的多，当晚围观者就少了不少……至于那些抗议自己当街对空喷火像是耍猴戏一般丢人要求涨雇佣金的请求。没人在意。

    而一旦我们走到人烟稀少的地域，还总是会有人想试试这一夜暴富的机会。有的大型佣兵团平时是雇佣兵，这个时候就化身流窜的马匪爷盗，我们也算是见识了那些下九流的埋伏技术。

    摆个摊子卖“药酒”的，挖塌大路打伏击的，趁着夜色玩夜袭的，最有趣的，大概是两位我们之前还喝酒聊天谈的情投意合的领主，成了我们击溃的盗匪中的最值钱的俘虏。

    而在审讯之后。居然发现还是惯犯。

    白天是领主，晚上是大盗，这可是大忌，一旦传出去，还有谁敢在这样的领地生活，做领主混到这份田地上，实在太过丢人，不仅领主的声望会遭到打击，连他们的国王一样会被质疑。

    于是，我们很体贴的为这些领主的上级考虑。写了一封措辞诚恳的信函，担保为了贵族的荣誉，会提他们保守秘密。而那个国家的国王也很是感动，送来了大量的财宝和粮食来表示“感谢”……咳咳，这你情我愿我的，怎么能说是胁迫，明明是自愿劳军。

    整个位面都处于风雨欲来的特殊时间段，一路上，风波倒是不断，但由于人类王国彼此间开始抱团取暖，更因为护送的舰队实力够强。也并没有遭遇什么可以翻船的巨浪。

    半个月过去了，我也勉强可以起床了。虽然魔力还是空空如也，但至少不用整天躺在处理公务了……以我难得的勤勉发誓。这半个月我至少干了三年的活，所以……我在想办法休息三年吧。

    行政的事情我并不打算怎么干涉，实际上他们也干的不错，一套勤勉的行政班子加上稳定高效的行政制度，绝对比拍脑门出歪主意的天才靠谱的多。

    我的主要精力，还是放在大陆的战争和岚盟的备战之中，毕竟，这方面的事情我还是比较拿手的。

    但面对足够活埋我的诸多战报，我倒是也没怎么上心，反正与己无关自然就可以高高挂起，而之后在相当长一段时间内，战报的数量都会暴增，这种让人火大却无能为力的东西，稍微了解一下就够了。

    唯一吸引了我的兴趣，或者说聚焦了整个位面目光的，就是拜尔和亡灵的战场了，五位亡灵大帝同时登场的豪华阵容，那一举一动都吸引了所有的目光。

    所有人都在等小妖精的情报和评价，但可惜的是，小妖精们最后给出的东西很有些让人失望。

    “所有亡灵大帝的评价都在五星之上？就这么糊弄过去？”

    而接下来的情报就太让人失望了，小妖精们给出的信息太过模糊，这样的评价只是说明他们肯定比弱等神祗强，但谁都知道亡灵大帝的实力在弱等神之上，这样的评价等于没有。

    但当具体的战报一出来，就知道不能怪小妖精们，只能怪那些古老亡者的老奸巨猾了。

    他们在战争之中根本没有表现出什么特质和稀有能力，所做的大概就是不断大范围召唤亡灵，然后一些常规的负能量死灵魔法攻击，了不起就是一挥手召唤的亡灵比普通的亡灵君主多几百倍，死亡一指之类的负能量攻击魔法可以当陨石术来砸城池。

    而谁都知道，亡灵大帝危险的是他们的特质，是他们达到造物主上限的研究领域，显然，这些狡猾的古老者不会这么容易露出他们的底牌，而由于他们的时代太过古老，常年蜗居在自己的领地，小妖精们要查出他们的底牌，仅仅是查阅过去的资料是不够的。

    恐怕只要寻找古老的当事人，这也不是一时半会的事情，而考虑到这么多年的进步，就算有资料也只能作为参考。

    而没认真就用亡灵海平推了雨夜要塞，也从一个侧面证明了这些家伙的底气，至少我个人是一点都不想面对他们。

    我的确是想和塔力共和国和谈，若是可以谈得拢的话，可以省很多事，但可惜的是，和平往往在硝烟之后才显得有价值。在他们确定自己的目标无法达成前，谈判是毫无意义的。

    摊上了笔记本，我深吸了一口气。起来关上了窗户，初冬的寒气让人觉得精神抖擞。但对刚刚才能起床的病人却有点伤。

    记日记算是我个人的习惯，不仅记录过去，也可以提醒我最近是不是做了蠢事，是否需要弥补。

    而现在的笔记本之上，最重要的几个目标却毫无着落。

    “完成圣剑黎明的材质收集——等悲风的消息，考虑到实力差距，成功率不大，但我怎么觉得他一定不会失败。”

    “以恒为渠道。和塔力共和国进行沟通，尝试共存的可能性——已失败。”

    “完成和拜尔、奥兰的三国盟约，敲定来年的军火、粮食买卖——已搞定。”

    这个后面却多出一条划线和补充，“考虑到现在战争造成的影响，不能指望他们一定能够履约了。可以考虑到找自然阵营弄，但欠那群家伙的人情会很麻烦。”

    “在海伦特的帮助之下，完成和地表龙族的和解——已失败，这群老顽固，一见面就喷我。”

    “继续黑圣堂教会，打击圣光之神的信仰。消弱他的能力——正在进行，但还看不到他被消弱的迹象。”

    “帮助海洛伊丝完成北地精灵帝国的建国行动——正在进行，选址都是大问题。希望能够在两年内敲定吧。”

    “完成寒冰位面的开放和新盟约的建立……”

    零零散散数百个任务摆出去，大概每一个都被当做史诗任务吧，但诸多事务之中，有一个却最是重要且麻烦。

    “查清卡文斯的目标——？？？”

    只有这个任务，我打了三个问号，因为根本不知道该从何处开始查探，

    这家伙的行踪成谜，只知道他现在在查找真正的深渊，若顺着这个线索去找。说不定反而会帮了他的忙。

    我已经委托了小妖精、法师之国真理议会、岚盟情报部门，但除了小妖精之外。其他的我都没怎么抱期望，就是小妖精们。恐怕能够获得线索的几率也不高。

    复活辛西娅？先不提双方的仇恨，我不认为卡文斯会这么没创意，复活个大魔王来灭世，然后被勇者打败什么的，这是多么古老的套路，要毁灭世界为啥不亲自上阵，再说了，毁灭了世界大家一起完蛋谁能够获得好处，感觉那些童话故事一点都不合逻辑。

    吞噬进化？就算不提那可悲的成功率，说实话，这也不是他的作风，他在某些方面和我有些相似，只会信任自己一点一滴获得的力量，完全属于自己的才是最真实的，光想想要吞下什么，以后体内还有一什么奇奇怪怪的生物存在，还和自己的血肉融合到一起，就觉得很是恶心。

    退一万步说，他真的渴望不择手段变得强大，而且愿意用吞噬的肮脏手段进化，也没必要这么麻烦找造物主，直接找主神级的麻烦不就够了，他现在应该还没到顶吧。

    当诸多可能性低的缘由排除，再离谱的答案也可能是真实的，我隐隐约约有几点可能的猜测，却需要情报佐证，有些可能，让我觉得不阻止他都无所谓，但有一个，却让我会不惜一切代价阻止他。

    “希望不是那个吧，卡文斯没那么疯吧……”

    “……按照你的画风的话，不是那个你最不想，就是那个吗？”

    看着在边上打着哈欠的死猫，我有些无奈了，这家伙还真是金牌乌鸦嘴。

    不过，这些天我还是稍微有点感激她的，自从我使用禁咒魔力、体力都透支之后，她就始终在我身边守着，为我保驾护航。

    而稍微让人有点伤心的，却是明明我受创这么重，却依旧有不少人来“看望”我……事实证明了还是不少人想我死的。

    外围的防御力量能够对付普通的弱者，却无法根除顶尖强者的渗透，这几天打发走的顶尖强者都快三位数了，超凡者和神祗窥视的杀气腾腾的目光都感觉到十几次，若不是我们这边隐隐约约也有冥府诸神护着，大战在即双方都保持克制，早就开始神战了。

    而且有能力以各种方式到我面前的，只有四位，但若不是海洛伊丝在，恐怕我也已经死了四次。

    死猫恐怕真的和亡灵大帝只有一线之隔了，那些看起来都是半神超凡者的家伙全部被她轻松打发，尸体都直接吊在战舰的主炮炮管上，用来警告后来者手艺不精就不要来送死了。

    我根本懒着问对方是谁派出来的，想我活着的不少，想我死掉的更多，多的都不用计数，现在摆明了是禁咒后的虚弱期，行踪又这么招摇，那些有仇有恨的自然要抓住机会，没仇没恨想出名想出人头地想要悬赏的也不少。

    “龙族、精灵、矮人、人类、兽人、亡灵，你就算问了也没有什么意义吧，这种族分布就说明想你死的大概是全世界……”

    海洛伊丝的话语让我想起来了，作为一个当世唯一使用复数禁咒还活蹦乱跳的禁咒法师，任何一个非盟友的国家势力都不会期望岚盟掌握这样可以反复使用的大杀器吧，不，考虑到这年头盟友就是用来卖的，连盟友都不能太指望。

    既然大家都想我去死，我的心思也都懒着去猜了，反正一些意外的收获，算是一个让人颇为满意的意外礼物。

    “寒冰系的传说武器有着落了？”

    这倒是一个意料之外的惊喜，却也在情理之中，还有什么比释放寒冰系的禁咒更加能够触碰寒冰元素的极致。(未完待续。)

    ...







  



