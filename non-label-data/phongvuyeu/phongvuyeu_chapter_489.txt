








    即使是高空也不是没有危险的，普通小国的空域对于大国的空骑士简直不设防，但随意进入大国的空域，也就等于挑衅该国的防空部队和空骑士团了。

    但最危险的，却是一不小心进入了法师塔的攻击范围，就算是巨龙遇到了准备充足的法师塔，也很容易栽跟头的。

    优质的法师塔可以让法师的法术十倍增幅，一个大范围的高环魔法在数层加成后变成小禁咒完全可能，被法师打下的巨龙为数不少，这也是巨龙们会对施法者保持基本的尊重的根源。

    而据我所知，拜尔王国各个主要郡县的首府都有这样的防空区域，甚至连稍微强大一点的领主都会招募*师构建防空区域，当然，他们防备的未必是外敌，毕竟超级帝国的大领主的财力都不会逊色小国家的国库，养得起空骑士的不在少数。

    正如神权和王权的永恒争斗，王权和地方领主的自治权，也永远处于矛盾之中。

    而若是王室处于极度弱势的时候，地方领主往往会先相互攻占，夺取对方的领地和领民，或许他们用的理由只是“你家那个小子出生庆居然没有请我”“你老婆勾引我二舅子”之类的扯淡，但若是王权失去了权威的时候，这些谁都不会信的理由已经够用了。

    王权弱，则内乱不可避免，而当某个占据了国家大部分势力的领主获得胜利后。新的王室也应该诞生了。

    而新生的王权既然是用实力登上王座的，自然就天生集权，王权就自然进入了强盛期。但世上没有不落的皇朝，代代明君更是不可能，只要他们依旧执行分封制和血统继承制，新的更替只是一个时间问题。

    很明显，这是一个让人不快的循环，但这样的社会结构让斗争和吞并永不停歇，让普通民众饱受战火威胁的同时。也不能否定它对社会进步的贡献，至少这是是一个天然换血机制。愚蠢或倒行逆施的君王，终将倒台。

    考虑到未来的发展路线，我倒是对这些政治制度进行过研究，却没有打算来个“解放农奴宣言”“打土豪分田地”之类的。甚至没打算进行律法之外的社会深层次改革，因为在当前地域广阔、生产力落后、野外魔兽过强的当下，这种领主对自己范围内的领民完全负责的制度，看似落后，事实上却是最适合当前社会现状的。

    “普天为公？所有人都该听国王的？既然都是王室的了，那么你就去找王室去保护你吧。”

    领主对领地、领民的所有权和财富占有权，也是让其组建战力，保家护土的原动力，那最初的骑士精神。不就从最基本的忠于领主保护领地来的吗。

    高尚源于平凡，对自己的所有物的保护无可厚非，在整个时代已经确定了某种基调后。跨前半步是天才，跨前一步就是会被绑上火刑架烧烤的傻子。

    若真有个穿越者上来就搞了一个完全的中央集权大国和郡县制，用轮值地方官换掉了领主，等待他的绝对不是大丰收，而是整个国家的分崩离析。

    或许中央的某项政令到达偏远地区就要数年之久，上令下达到基层意见不知道变成了怎么样。而集权导致的人口过负荷聚居，是当前时代根本无法解决的问题。

    仅仅太多人口聚居可能引起的瘟疫天灾和饥荒就是无解的。稍微思量一下，就知道这么做等于作死，好心做坏事就是这样来的。

    当然，若是弹丸小国试试还是可行的，若是技术发展到一定程度，逐步推行城镇化和现代化，医疗、通讯、交通、农业、工业等随着产业革命上升数个档次，就算我不做什么，时代也会自然变更的……貌似一不小心泄露了什么，嗯，我承认我个人的确心动过，但要斩断这王权更替的轮回，却不是一朝一夕的事情。

    当然，也有少数的例外的，比如国土太小的国家，神权和王权统一的宗教国，时刻都有外敌入侵的威胁中的边境国家，他们出于各自的缘由，政权相对稳定，才有了千百多年的传承。

    咳咳，历史的轮回就暂且不管他，反正都已经轮过这么多次也不少这次，我还没打算喊出“人人生而平等”“所有的财富属于每个人”的口号，或是成立什么镰刀斧头党，成为全世界公敌的打算，有多大的力使多大的劲，还是先看看当下吧。

    拜尔王国现在王权处于强盛期的时候，诸多地方领主自然不会相互攻伐，相反，他们还会团结起来，抵抗皇室对于集权的渴望，既然完全集权是现有条件下没可能实现的梦想，这是一场注定没有永远胜利者的斗争，双方的争斗将永远持续下去，直到新的王室产生。

    如今的拜尔处于历史巅峰期，奥罗斯甚至可以让其他领主把自己的嫡长子送到王都来“学习”，而对其他领主私兵的整编也取得了一定的成绩，但若想让所有的领主乖乖听话，却至少还要多百年，中途还要百战不殆内外不乱，除非他是开了金手指的亡灵主角，否则他是肯定等不到了。

    如今的拜尔虽然名义上统一，但若是按照严格的标准来划分，还是一块一块的分封领地，计算一个国家所拥有的战力，往往就是指的是该国皇室极其领地的附属军队，皇家骑士团也等于国家骑士团，但若是大型战役的话，恐怕还要加上各个领主的私军，毕竟响应国王的号召，派出私军参与国战也是领主的基本义务之一。

    别小看是私军，他们的训练和装备都是某个领主家族传承的根基。特殊装备和兵种往往因地制宜，比王国正规军还强的私军多得是，而往往由于王室对领主的限制。被限制了私兵人数的大贵族会试图以质取胜，平均单兵战力远超皇家骑士团的也不是没有先例。

    私兵的人数限制往往和爵位有关，但还有民兵、民兵预备队等钻空子的办法，虽然一个个个体看起来不多，而一旦累积起来，绝大部分国家的贵族私军累积起来的战力都是王室数倍以上，才能对王室造成制约。毕竟，从某种意义上来说。这个时代的封建王国的王室本身就是该国最强的大领主。

    这还是正常的领主，而若是镇守边关的大领主，那手下的兵力就不可能被限制住，这样的大贵族。也很容易成为新的王室候补。

    而一路上，我也算是见识到了拜尔王国到底有多深的底气了。

    基本到任何一个区域，都会有空骑士主动跟进，或是狮鹫骑士，或是飞马骑士，连龙骑士都遇到一位，居然还有骑着蝎尾狮的施法者，或是一个小队，或是三两只。普通小国都养不起的空骑士却是拜尔的领主们的最基本配置，我居然还看在两个在云中若隐若现的塔楼。

    他们看到前头的是龙骑大公的蓝龙，却没有径直离去。只是默默的跟随，直到离开了领空，往往是地处边境线的时候，新的空骑来了，之前的空骑才转身回去，而这礼貌又保持距离的态度。却不仅仅是针对我们这些外来者，一路上居然没有一个人和龙骑大公打招呼。这从骨子里透出股不寻常。

    名义上只属于皇室的龙骑士团还是有在各地域的自由通行权，但这些默默跟随的空骑士或许阻拦不了龙骑士做什么，但至少表明了一种态度——“这地方是我的，你可以来，但必须在我们的监视范围内。”

    这显然有些过头了，和我当初听到的很有些差距，似乎，随着皇帝陛下的大限将至，这些领主们又有了新的想法。

    不过即使我还没有抵达王都，都可以猜出点什么了，无非是皇子们太多了，这一场内战似乎不可避免，但一旦打起来，不管谁是最后登上王位的胜利者，王权的权威自然会大幅降低，拜尔四十多年前的领主压迫王室的时代，似乎又有到来的可能。

    而对于龙骑士的防备，或许不单单是政治上的表态，也多少有点实际意义。

    “谁知道你是不是来记录部署和军事部署情况，为之后可能发生的战争做准备。”

    至于我们这些外来者？完全没有被盘查，或许，由于有这个带路人，我们也被看做了龙骑士团的一员，而我们的新伙伴，也顺利的和我们同行。

    新伙伴？嗯，之前不是我才随口说了句，就突然有龙卷暴风了吗？实际上那有那么巧，那暴走的风暴在我们面前就重现化身为一个熟悉的身影——风元素之神卡姆迪尔安。

    “罗兰阁下，我是专程来找你的。有些很重要的事情我要和您细谈。”

    在干掉厄姆迪罗肯后，他就逃之夭夭……匆匆离去，但现在却主动出现在我的面前，显然有事情找我，而和过往一个个小虫子、愚蠢的凡人不同，但他明显把姿态放的很低。

    这些古老存在就是如此现实，你不拿出证明你是对等存在的战绩出来，他们是不会正眼瞧人的。

    我当然很高兴见到卡姆迪尔安，毕竟我现在正在找风元素之神的神物，干掉他……问问他说不定就有收获了。

    但仔细看过去，却只能无奈的放弃了某个后备方案，毕竟对方出现在我面前的只是化身，这种玩意动不动就自爆不说，还很难爆什么好装备，这种难打又不掉东西的波ss还是算了吧。

    但现在在赶路之中，显然不是聊天的时候，于是直接要他跟随就是，但奇怪的是，他却急迫的想说些什么，于是，我就随口问了句，而他大喜若望的直接询问了。

    “阁下，我相谈的，就是您在奥兰那里做的，那个驱逐神力的亵渎牧师和反圣骑……”

    “啥？亵渎牧师？反圣骑？”

    ------------------

    当拜尔西南的战区正在上演泰坦战役的时候，奥兰的海战战场也取得惊人的战果。

    蕾妮派出的四灵剑使和送葬舰队的配合取得了惊人的战果，战舰群横冲直撞，而被冲散阵型的海族就会被分成三队的四灵剑使收割，一路推进战果显著。

    事实上，这套战术小队和舰队的配合战术，本就是用来配合北极光号和它舰队中的七艘超重型战舰的，现在拿出来正好。

    而在实战之中，小队战术的问题也暴露出来了，比如说个体战力过弱导致不敢硬拼，部分装备由于制造工艺不过关，出现了漏水和触电等多项问题，非战斗损失在增多。

    但蕾妮的指挥还是很得当的，有平均战力极高的极光骑士做掩护和支援，东岚的战果显著，虽然损失了一些装备，但人员损失并不多。

    而海族的援军虽然赶到了，却依旧挡不住黑色的送葬舰队的缓慢冲锋，双方进入了绞肉机式的消耗战，但一方堵上了一切，缺乏斗志和牺牲精神的海族们自然节节败退。

    仗打到这种地步，虽然人类方的损失也不小，但还是达到了一开始制定的战略目标——抵达水下基地所在的码头区。

    而到了那里，舰队们开始横摆转向，进入了布防状态，潜水者们和海洋准备开始水下作业，但一个意外，却让整个战场的形势彻底扭转，也让卡姆迪尔安不远千里找上了罗兰，毕竟，这场战争中展现出来的东西，对他这样的古老元素之神来说，意义巨大。

    “那些黑暗精灵，出手了！她们扭转了整个战场的形势！”(未完待续)

    ...







  



