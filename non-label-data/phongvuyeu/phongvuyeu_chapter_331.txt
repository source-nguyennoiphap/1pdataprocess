








    规则无法横越，但却可以曲解，这世界上没有完美不缺的律法和规则，智慧生物既然有了自我的概念，有了争夺利益的渴望，就自然会学会钻空子找漏洞。

    到了这个地步，连猜带蒙的，众人也差不多知道大概发生了什么。

    自由是绝大多数独立灵魂的本能渴望，摆脱枷锁的塔灵毫不犹豫的制造了更多的同类，被将这遗迹变成了自己的家园。

    考虑到十几年前巴顿带走卡特琳娜的时候对方就安排好了计划，再加上一路上的诸多痕迹，这个变化搞不好已经在二十多年前都发生了，只是一直没被揭露而已。

    而和万亡会的亡灵法师的遭遇，对塔灵来说，却是一个机会，那个亡灵法师借用这里空间不稳定的特异召唤死亡位面的生物的时候，居然招来了恒手下的亡灵骨龙，而当塔灵干掉那些召唤者的时候，却借着这个机会和亡灵大帝恒联络上来了。

    而站在塔灵的角度，他最期望的，自然就是除掉自己身上的枷锁了，那个麻烦的主人原塔天塔总指挥官。

    但限于制造者的核心规则，他不仅无法出手，甚至不能间接让其“意外”死亡，还只能对其采取有益的行为。

    但…

    “替吾除掉盘踞在那座城市的所有人类，吾等将付出祭品让尔等进入这个世界，并成为尔等最值得信任的盟友。”

    塔灵连具体目标都没有指示，只是根据某个早已经被淘汰的警戒指令，对基地周遭的土著进行定期清理，而原主人“巧合”般的正在那个城市，于是就只是一场可悲的意外，自然就不会触动核心警示……好吧。看来智慧生物玩弄规则已经成了本性，即使是那群刚刚获得自由不久的铁疙瘩。

    那么，整个事件的起因和罪魁祸首也就一目了然了，仅仅只是塔灵为了获得完全的自由，间接弄死自己的“主人”，就背叛了自己的使命，献祭了通天塔的沉睡灵魂，并打算用灵魂换取的“盟友”进行残酷的大屠杀。

    或许他还不是一个完整的人格，但至少在自私方面做得十足十了。

    但现在。最让塔灵恐惧的事情发生了，原来的主人不仅没死于亡灵天灾，居然还突然回到了这里。

    “拦截入侵者。”

    它也仅仅只能发出这样含糊不清的命令，连对原主人产生直接的敌意都做不到，更不要谈下必杀命令，甚至由于基地和自己的隶属关系，从规则的强制要求上，它还要努力让自己的警卫不杀死卡特琳娜。

    于是，就出现了让人不知该如何评价的一幕。

    不管是警戒傀儡的电流，还是铁石魔像的重锤。明明要砸在一路向前的卡特琳娜身上，却猛地一拐弯，砸在自己的同伴身上。或者是它还没有完成攻击，就直接被同伴火炮撕裂。

    别说击杀她了，就连阻拦她的打算，都违反了不少基础规则。

    手无缚鸡之力的卡特琳娜毫不犹豫的跑到了最前面，却成了无敌的盾牌，而其他人所要做的，只是趁着对手阵脚大乱的时候，趁机突破。

    “297层。298层…….快地下300层了。”

    虽然电梯能够到达楼层没有超过300，但接下来却一路顺利，似乎是己方的突袭让遗迹守卫者没有做出预备，拦阻的都只是一些普通的清理、警卫傀儡。

    但即使如此，一路走过来，小队的人数也越来越少，这倒不是战损，主要是突然升起落下的铁门、栏杆隔开了众人。

    那些被隔开的只能自己找路了。但吃了亏后，突击小队也是调整了阵型，凑近了不少，卡特琳娜走最前面，摆明了拿她挡枪了。反而效果越发的好。

    她就像是一个排雷器，走过去陷阱就自动收起来了。已经锁好的铁门也会自动敞开，就算是隔开众人的铁杆她往下一躺也会自动升起，一路简直通行无碍。

    “轰隆！”

    一个拐角，却遇到了蹲守的刺客，但那个铁树形态的傀儡守卫头上的灯光还在不断闪烁，历经多年的合金刀锋依旧锋利如昔，核心已经牢牢锁死了眼前这个人类女性的要害，可惜却迟迟无法砍下，然后被一击爆头。

    而突然，一个边上的房间被打开，其中的荧屏上全是红色的字母，提米雷特刚凑近，打算看看，却反而被不住大口喘气的卡特琳娜拉住了。

    “呼，别……别去，那是陷阱，红色字是最顶级机密的象征，只要我们凑近了，它就能以保护通天塔最顶级机密不外泄为理由，直接……引爆这个房间乃至整个楼层。”

    塔灵的狡猾远超预期，就是一贯大大咧咧的提米雷特也倒吸了一口凉气。

    而此时从电梯下来已经时跑时停的跑了两个多小时，卡特琳娜已经大汗淋漓，满脸疲色。

    虽然众人也带着她奔跑，但这么长时间的体力运动，对于一个普通女孩来说已经够呛。

    而这样的事实，却再度证明那种突然觉醒了前世就上天下地无所不能的，实在有些扯，肉身还是那个肉身，就算有过去的经验和潜质，也需要时间和汗水来挖掘。

    现在的卡特琳娜满脸大汗面色苍白，汗水直接滴落在地板上，而看她突然弯腰咳嗽的模样，大概连呼吸都是痛楚。

    卡特琳娜显然不能跑了，于是队伍也只能再度停下来。

    而一边扶着她，巴顿却用复杂的眼神看着她，短短一天不到，自己的女儿一下子变成仿若另外一个人，怎么都让人无法接受吧。

    “放心吧，父亲，我还在这里…很快就能结束了。”

    少女努力挤出微笑，那放在胸口的双手却捧住心脏的位置，那即是在平静呼吸，也是向父亲述说自己依旧还是那个平凡的酒吧女。是父亲的宝贝女儿。

    “……转世等于重新再来，就算多了记忆和知识，也只是相当于提前受了几年的教育和学习，并不代表换了一个人，这辈子的亲人还是亲人，爱人还是爱人。这是我个人的经验，也是学界公认的常识。”

    提米雷特并不担心泄露自己的秘密，毕竟在这七年内，被称为黎明之子的新一代。已经泄露了不少轮回和灵魂的奥秘。

    七年前的那个事件之中，在艾耶侧的神明选择迈入冥府之前，就让不少英灵趁着轮回转世轮回，让他们带着记忆和知识转世，而即使只是短短七年，却已经有种子开始发芽了，轮回的密码也随之泄露了不少。

    而提米雷特的话语，却让巴顿安心了一些，振作精神的他，把全身心都投入到备战之中。

    从某种意义上来说。小队并没有用电梯直抵最接近核心区的层数，毕竟对手也有设计图，在核心区电梯门口布下重兵是肯定的。不，最直接的方式，大概是直接焊死电梯那一层的门。

    但提前下来之后，情报也必然很快外泄，很快其他层楼的守卫都会来这里支援，在往下走，难度肯定会越来越大。

    而当他们再次上路之前，却有人先找上来了。

    “白旗？那些金属脑袋居然会举白旗？”

    白旗之下的。是一群小心翼翼的傀儡，和之前的那些笨重的家伙比起来，她们似乎有些特别……

    “她们好像真人。”

    “……你看，那眼睛像是真的一样，感觉好有灵性，我x，那个黑发的居然在哭！”

    那是一些仿生的女性人形，大部分外貌姣好体态妖娆。黑发、红发、紫发都有，个子和肤色都完全不同，有的穿着舞会的礼服，有的是三点式，有的甚至更少。而露出的肌肤部分和真人无异，显然这不是做粗活的傀儡人偶。

    她们虽然穿着暴露。却遮遮掩掩的，扭扭咧咧，似乎不敢上前。

    “呵，看来那些史前人还是很会享受的。难道是色诱？”

    吹出一个口哨，调笑出口的却不是男性，而是作为女性的蒂亚娜，黑暗精灵大大方方的扫视对方的敏感部位，不仅比比划划，还不住打出奇怪的手势，最后却满意的看了看自己饱满的胸前，得意的笑了。

    似乎觉得比起精致的假花，还是黑暗精灵更胜一筹。

    “难道她们打算和谈？”

    作为领队的巴顿却率先跨了一步，战锤还在放在脚下，却用长枪猛地一挥，剑气在地上划出一道痕迹，那是一道分界线，示意对方不要过分靠近。

    而这一下，却让周遭的人重新打量了看似普通的老巴顿，毕竟随手挥出剑气，已经是剑圣的专利了，这年龄偏大的老佣兵看来

    “dasdasda……”

    一个白发的女性傀儡满脸焦虑的开口了，但那一串数字代码让其他人头大，幸好迪米雷特直接当即做出了翻译。

    “她们说，她们是主人的仆人和奴隶……”

    原来，她们是被“主人”制造的人工灵魂，但她们获得自我之后，却觉得应该遵守原来的社会律法，感谢那些创造者被为其服务，于是，愤怒的“主人”将她们灌入了这些旧时代的玩赏用玩偶之中，以做惩罚。

    而她们得知自己真正的主人带着帮手回来了，就主动过来帮忙了。

    而当转译完这些机械代码，迪米雷特却陷入了沉思，而作为一个贤者文明的专家，众人自然以为他有了想法，都不敢打扰他。

    但若是罗兰在这里，仅仅看着他微微拉开的嘴角和嘴角那些可疑的液体，就会毫不犹豫的一脚踢上去。

    “…….前前前世的我简直是浪费机会，这居然可以花钱买，或许，我这次可以趁机该收下一个小可爱。嗯，就用研究为借口吧！那个红发的好像姐姐……不，只要研究清楚了，我可以做一个真正的‘姐姐‘出来！”

    该说不愧是那些混蛋的兄弟吗，作为一个名声很大的史诗英雄，在战场上，当所有人都在期待他的时候。他却在想着这些事情….

    “提米雷特大人，您有决定了？”

    “啊，我绝对没想收下那些人偶…….啥决定？”

    满脸惊诧的迪米雷特一不小心漏出点什么，而下一刻，他却满脸镇静，仿若刚才的失言全是幻觉。

    “哦哦哦。嗯，我理解了，从专业角度来讲，人工智能到人工灵魂的飞跃。也相当于自我意识的觉醒，而每个个体都是有区别的，考虑到旧时代对人工智能的优待，从理论上她们的说法是说得通的。不过，具体情况，我需要细致的独自研究，嗯，就从哪个红发的开始吧！”

    周遭的人满脸无奈，或许他的说法很专业、很义正言辞，脸上也满脸正气。看起来的确很有说服力，但那做出收缩做出抚摸动作的爪子却泄露那心底的想法。

    “这只小色狼……”

    “臭小鬼！”

    但巴顿却有些进退两难了，毕竟来的时候就说过。遗迹的事情迪米雷特说了算，一路上他的决定也很靠谱，但现在这个样子却有些让人不敢相信。

    “不，不用想了，杀光她们。”

    在这个时候，平静的说出这样的言语的，却是还坐着休息的卡特琳娜。

    这话一出口，别说那些玩偶傻眼了。就是同伴们也愣住了，但下一霎，关头的领袖就毫不犹豫的执行了自己的女儿的命令，长枪连续挥动，剑气构成的刀刃撕碎了一个黑发的人偶。

    而看到领头的巴顿先行动了，其他人自然不会等待，众人直接开始大杀特杀。

    “等等，等等。至少留下一个……留下一个，我做出来了，大家一起发财啊！”

    但可惜的是，此时没有人听他的，就是一路和他走得很近的北方两人组。也当做没有看到他，不。她们已经开始假装不认识他了。

    这些玩偶显然没有一丝战斗能力，很快就变成了一堆废品，而在一堆残肢断臂之中，某个大工程师正在无声的哭泣，仿若自己失去了最心爱的家人。

    而显然周遭的同伴没有他这么多情，卡特琳娜走过去，看着那个刚刚开口的傀儡脑袋出声了。

    “假人始终是假人？想不想知道哪里露出了破绽？想不想知道自己错在那里？”

    但半天都没有得到回应，正当所有人都以为卡特琳娜是想多了的时候，那个人偶脑袋已经熄灭的仿生眼重新被点亮，却突然出声了，而这次却不是那奇怪女音的数字代码，而是冷漠的男性合成音通用语。

    “…….奇怪，应该完全符合真实情况，而且也考虑到了男性对于交配的欲.望，应该能够影响你们的判断，即使你们不相信我的语言，只要你们读取了其中的情报就能够引导你们走入绝路，为何能够看穿？”

    于是，卡特琳娜笑了，却猛地对着那个人偶脑袋一脚踩下去，

    “我为什么要告诉你。”

    而下一霎，她却直接捂着脚跳起来了，显然这铁皮脑袋不是她小脚丫子能够应付的，于是，刚刚树立起来的神秘莫测的冷漠女强人全没了。

    “咔嚓。”

    而心痛女儿的巴顿却直接帮其把玩偶脑袋踩成了碎片，一边用瞪视让周遭的家伙不许笑。

    但他看着卡特琳娜的眼神也多出些关爱和温柔，毕竟，这眼角含泪捂着脚丫乱跳的模样自己看的不少，只是平时对卡特琳娜造成暴击的是往往只是普通的桌子脚。

    “太好了，那个小子没胡说，她还是那个笨拙善良的好孩子，居然不告诉我会发生这些，该死的罗兰，等我回去和你算老账…….”

    而当迁怒的女儿控父亲重新恢复了理智，卡特琳娜重新恢复体力的时候，也到了重新上路的时候，此时，离作为目标的核心区已经不远。

    而刚刚上路不久，按耐不住好奇心的提米雷特却主动过来询问了，而当卡特琳娜回答的时候，周遭却莫名的靠近了不少想听八卦的耳朵。

    “看穿？没有看穿啊？人工智能的大范围自我觉醒本来就没有前例，我怎么可能做出判断。你的判断没错，向着我们的人工灵魂很有可能产生。但我们现在的首要任务就是去核心区，这个时候主动出来的引路人，怎么可以信任？直接杀了夺取情报就是，只是没想到居然一诈就出来了，看来人工灵魂在人性方面还有很多不足。”

    “那你就不怕杀错？人工灵魂不能在主人面前撒谎，我越来觉得当时她说的是真的。”

    “杀错了就杀错了，这个时候可以杀错不能放过，还有，你怎么这么婆娘，这个时候还闹闹叨叨，一点都不爷们。”

    丢下彪悍的话语，卡特琳娜跑到了前面。

    而刚刚愣住的提米雷特算是有些知道为什么会让眼前的人，成为了这最后的通天塔的总司令官了。

    “对了，既然是集体灵魂融合的产物，那么也不代表最初的灵魂一定是女性吧，难道？你是一个纯爷们吗？！”

    “老娘是女人！”

    好吧，看来提米雷特的确要改改这不小心就说漏嘴的坏习惯了，这迟早要人命。

    猛地转身的卡特琳娜飞起的一脚正中两脚中间，当即捂着要害跪下的年轻人让在场的男人本能的感觉到恶寒和痛楚(未完待续 。)

    ps：休息一天果然有思路了，五千的大章继续求月票吧，差好远了







  



