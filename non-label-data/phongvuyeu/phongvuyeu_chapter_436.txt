








    水元素位面，又被称为甘蓝之星，她整个位面就是无数层海水潜流构成的混合深渊，和其他固定坐标的元素之地不同，甘蓝之星有无数的暗门和空间通道，谁也不知道在某个水层后面，是不是通向某个未知位面。

    这片区域虽然被称为甘蓝之星，但实际上却只有最表层的水域是清澈的甘蓝色，在下层的水域之中，灰黑色、黑色的水域让人根本无法探知深浅，事实上也没有任何存在知道这颗水之星到底有多少强大居民和宝物，毕竟诡异的暗流和自然环境可以淘汰任何贫弱的来访者。

    赖文咯单达，很绕口的名字，但在元素的语言之中，却是“中转站”的代名词，这是一片浅蓝色水域的名字。

    这里的海面上甚至有一些明显来自异界的船只，而这片海域也是甘蓝之星和其他位面的生物接触的中转站，也是少数具备空气的水域。

    是的，这里依旧是水域，甘蓝之星没有没水的地方，整个位面都是由水构成的，而在有的地方却连那些鲸鱼之类的水生动物所必须的氧气都没有，或者是干脆在零下三十度以下的魔域，相比之下，这片能够让普通生命存活的海域已经足够友好了。

    即使同样是水域，不同的海水成分、周遭水压、温度、重力、元素潮汐，都让每一块海域的居民完全不同，这里大概算是最佳度假群岛了。

    在“中转站”的那片水域之中，温度、空气较为温和，而在水下被人工隔开的无水区，也是异位面商人和探险家的据点，往日这里都是颇为吵闹的，而今天，这里的街道上却空无一人。

    那是因为水元素之主要在这里接见一位尊贵的客人，而这次接触却不想被任何一个人见到，于是事先就让水元素督军进行了清场。

    从某种意义上来说，艾伊洛斯是所有元素之神混的最好的一员，刚刚脱离封印之后，她就从新掌握了水元素位面，并带着手下在艾希搅风搅雨，除了水元素们野心不算高之外，更多的却是她的人望一直颇好。

    但此时，位面之主的脸色却很差，白皙的面庞虽然看不清表情，但颤抖的手指泄露了她的情绪。

    “这就是艾耶的意思吗？”

    放下信纸之后，艾伊洛斯的目光看向远处。

    在自己的世界的艾伊洛斯，肉身仿若实体白玉一般，外貌俊秀的超出了凡人能够理解的范畴，而此时，过于庞大的神力让其周遭的空气不住震荡，她的表情似乎没有变化，但无水区外延都掀起了剧烈的风暴，动摇乃至惊惶的心情和元素之神的权限一同尽显无余。

    而她面前的信使却不为所动，只看外表，那是一个俊秀人落魄的年轻精灵诗人，但能够在水元素之神面前如此平静的，自然也不是凡人。

    他是音乐和舞蹈的化身，更是吟游诗人和木精灵的守护神，而如今，这位强大神祗化身，却只是一个等候回应的信使。

    “你们要我去死！？”

    随着元素之主的怒吼，甘蓝之星的海面上，无数的波涛形成暴走的水龙卷，暗流化作潜龙拍打这个无水区的隔水墙，那碎裂的波纹已经带来了恐慌。

    在自己的主场，一个完整的上古元素之神并不畏惧任何一位神祗，暴怒的艾伊洛斯展示了自己的权能。

    但索罗却摇了摇头。

    “信里应该说的很清楚了，我们对你并没有敌意，只是给你提出一个建议。”

    “建议我去死？”

    即使依旧愤愤不平，艾伊洛斯却冷静了少许。

    “不，只是若你死亡了，雨之都至少失去了一半的能力，然后凡人们会借机攻击海族，‘她’的信仰源就遭受重大打击，我们就会按约定全力抹杀掉‘她’，而她的海洋神力将用来复活你的丈夫。而错过这次机会，我们将不可能也不会去复活你的丈夫了。”

    这些内容都在信件上写的的清清白白，有神名和神誓做担保，根本不用担心信上的内容是虚假的，但就是这种真实，反而让艾伊洛斯内心动摇、心神惶恐，她知道这是自己丈夫唯一的复活机会，自己现在只能在牺牲自己和牺牲丈夫的复活机会中进行选择。

    “没有其他的可能吗？我愿意付出一切来交换斯顿维拉的复活。”

    索罗没有回答，但此时，无言的沉默已经是最残酷的答案了。

    或许罗兰可以在事后复活海洋之神，或许到时候难度没这么大，但如今“计划”已经到了最关键的时刻了，任何一环都不能出现问题，艾耶侧却没有等待和“或许”的余暇，艾耶也不可能这个时候强行抹杀一个强大神力的神祗。

    水元素之神若愿意牺牲，那么就启动击杀风暴女士的计划，若不愿意牺牲，在元素位面击杀完好的元素之神比击杀风暴女士难度更大，这一系列计划就只能搁置，而考虑到现状，这一搁置，基本上就是永久搁置，海洋之神斯顿维拉就不可能复活……不，即使今后罗兰有机会复活斯顿维拉，有了今天的选择后，也不会让其复活了。

    所有的情况和条件都已经摆在了艾伊洛斯面前，要么她死，要么她丈夫不能活，或许她在答应风暴女士的条件，联合施法让雨之都化作凡人的噩梦的时候，却没有料到自己会有因为这件事陷入了生死两难的选择中。

    “…….我,我需要时间……”

    性格一向比较直接爽朗的艾伊洛斯，却犹豫了，深深的犹豫了，紧握的手捏碎了信函，心底的犹豫展露无遗。

    但索罗却残酷的摇了摇头。

    “我们最缺的就是时间，今天之内就必须有结果。”

    或许，最残酷的就是实话了，就是对女人没兴趣的索罗，此时看艾伊洛斯都有些同情了。

    艾耶洛斯沉默了，半响，她挤出了苦笑和回答。

    “…….我知道，给我三个小时。”

    点了点头，索罗起身告辞，如今的水元素之神情绪可不稳定，若是想泄愤什么的，自己莫名其妙的赔上一个化身可划不来。

    而和索罗所想的完全不同，独自留在大厅的艾伊洛斯却没有发泄和歇斯底里，相反，驱走了所有的手下后，她就很平静的坐在那里，然后抽出了一个小空间袋，从中抽出一把老旧的鱼骨梳，对着镜子，给自己梳头。

    而当骨质梳子遇到了水质的发梢的时候，自然从中穿过，艾伊洛斯先是一愣，然后苦笑着摇了摇头。

    白光闪烁，留在那里的却不是威势逼人的水之神，而是一位蓝发的肉身美人，柔滑的长发一直拖到地上，姣好的面庞精致而柔弱，娇小的身躯斜躺在那里，浑身只有贝壳隐隐约约的遮住了裸露的上身，却是她们的传统服饰，那带着粉红鳞片的鱼尾说明了她的身份——美人鱼。

    现在，她可以静静给自己梳头了，就如过去那个强壮的男人每天早上都会做的一般。

    那时候，自己每天早上就是这么静静的盘着尾巴坐着，等着背后的男人为自己梳头。

    “……有多少年了，你总是说，我这个样子太过柔弱，会被手下瞧不起，要想你变成强壮的大个子模样才能镇得住场面…….”

    梳子划过细滑的长发，蓝色的长发在半空甩起，点点水光在其中闪耀，就如长虹一般美丽。

    但随着雨水落下的，却是一根根独自飘落发丝和压抑不住的泪水。

    “……你总说我是个爱哭鬼，要我学会强硬，要不会被手下欺负的…….”

    这里没有强大的元素之神，只有一个因为失去了爱人而哭泣的柔弱美人鱼。

    她的泪滴啪啪啪的落在地上，刚刚化作银白的珍珠就被摔的粉碎，而在甘蓝之星的海面上，整个世界都下起了淅沥沥的小雨。

    “……还记得吗，你当时很生气，因为我放走了那个偷走我泪珠的侍女，你说我这样只会让别人觉得我软弱可欺……”

    泪水却没有停下来，整个位面的雨越下越大，而伴随着泪水散落的，还是水之本质，美人鱼的肉身开始变得透明起来。

    “…….我一直都很崇拜你，你是那么强壮、坚强，没有人是你的对手，你就仿若世界的主角，但我最喜欢你的，却是你内心的温柔。当年你还问我为什么我会看上你，我直接生气了……呵，你以为我生气是质疑我的感情？我生气是你忘记了当年那个在魔鲸口中救出来的小美人鱼…….”

    泪水模糊了艾伊洛斯的视角，但她却从袋中摸索的拿出了红色的记忆宝石，而其中出现的景象，却是那个比天还高的强壮身影，带着王冠的他带着自信的笑容环顾四周的海域，普天之下都是他的领土，他是真正的无冕之王。

    “……你说柔弱会被人欺负，要用力量和威严让人心屈服，可是，为什么做到一切的你，却依旧被最信任的手下背叛……..”

    即使身躯已经开始崩溃，艾伊洛斯伸出了已经变得半透明的手臂，轻轻的触摸着那个英气十足的男人，但手臂却从幻象中穿过，而少女看着自己已经消失一半的手臂和那同样虚幻的男人，却笑了，她似乎又在最后看到了那个人。

    一切都回归本源，美人鱼最后的语音在空空荡荡的大厅中回荡……

    “大个子，你的小美人鱼好想见你。”

    留在原地的，却只有一滩水…….

    从某种意义上，索罗的心情很复杂，一直期盼的目标终于到了实现的时候，自然有些开心，但却必须来执行这样的任务，就让人有些不开心了。

    当他刚刚走出大厅不久，看到外面已经是疯狂的潮汐和波浪，疯狂暴增的水压让无水区的防水墙开始崩溃，一切都出了问题。

    而感觉到背后更加剧烈的元素潮汐，索罗一下子惊呆了。

    “不对吧，怎么这么冲动，不是说三个小时吗？”

    他立马拔腿就往回跑，边跑还边摸兜，在掏出了口风琴、叶子、竖笛、橄榄油等一系列杂物之后，他终于找到了自己的目标。

    那是一颗白色的冰晶，其中蕴含的却是完全陌生的力量，那是来自异界的造物。

    “……若艾伊洛斯答应了，就把这个给她，或许，能够保住……算了，成功率太低了，就当一种尝试吧，情况可以的话，我们也不想斯顿维拉和我们拼命。”

    当时的罗兰是这么说的，但现在这个应该交给艾伊洛斯的异界之冰却握在了索罗的手上，他一边拼命挥舞，一边大喊。

    “你丫是超级慢性子水元素吧，怎么这么冲动。不带这么玩的，艾耶老大会撕了我的！”r1152

    ...







  



