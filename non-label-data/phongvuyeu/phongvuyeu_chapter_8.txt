








    发光苔藓，永远静静的挂在地下世界的岩壁上，让这些被放逐者的子孙，也拥有享有光明的权利。

    这样的植物，在另外一个世界也曾经有过研究探讨，其原理大概是通过转基因让萤火虫和苔藓的基因融合，让生物的荧光现象和植物的光合作用进行协同,实现低消耗的能量循环,成为一个自给自足的供光体系，作为地下的永续供光系统。

    它本身可以种植在矿坑内防止矿难，其衍生品光苔灯,可以应用到公共环境照明中。以光苔能源改善社会的能源结构……虽然说得如此之好，但技术难度实在太大，而即使能够突破技术难题，把植物和动物基因硬是拼到一起，谁知道会出现什么怪物，最终，还是停留在理论上。

    我是没有料到，当时在科学杂志上看到的纯理论探讨，居然在异乡化作了实物，果然，比起始终有所限制的魔法和科技，自然的进化是无穷尽的。

    若是按照地上贤者们的记载和研究，这种不仅会发光，居然还会发热的诡异植物，大概会有一连串让人听不懂的前缀和术语名词，但在地下世界，他有一个和它丑陋外表一点都不相称的好听别称——太阳花。

    每一块地下城，都注定建立在发光苔藓聚集茂盛的地域，而若是在地下城外，这样的天然有光区，更是会招来无数凶兽拼杀夺取。

    当然，近代有大德鲁伊发现了发光苔藓的种植方式，才让地下城的规模不再受发光苔藓，但要找到能够飞到云端之上的地下岩壁边的超级飞禽、大德鲁伊进行播种，实际上成本还是极高。

    或许，也就白金之城这样的天然重镇，才舍得花大笔金子不断增加发光苔藓的区域，扩大城区范围吧。

    而此时，随着远处的光辉逐步点亮，很快，这里的发光苔藓也会开始发光，新的一天，就此到来。

    肉身需要睡眠来恢复疲劳，让大脑停停机，而普通的亡灵根本没有疲倦这个概念，是不需要睡眠的，所以，我才半开玩笑的起了无眠者这个名号。

    但到了亡灵君主这个层次，大部分亡灵君主们又重新拾起了睡眠的习惯，有的说自己是为了恢复精神上的疲累，有的说自己是暂停身体机能更快更好的恢复魔力，但闲的无事的我做过研究，恢复精神疲累和补充魔力都有更好的办法，完全没有必要深度睡眠，让自己失去知觉，显然危险之中。

    通过测试，最后得出的结论是的确不少亡灵君主们如活人一般行动作息，肉体完善的，甚至能够恢复味觉、**，更多的，只是一种灵魂深处本能的习惯，然后肉体本能的模拟那些功能而已。

    因此，那种憎恶巨人、石像鬼、骸骨巨像之类人造亡灵生物，即使成为了智慧高阶亡灵，依旧完全没有生者类似的行动和小习惯，但有些好奇的我，却做了一个小实验呢。

    “亡灵这种非自然生物能够存在于世，往往也是怨气和仇恨的结晶，“不甘就此死去”也是一种**，若是纯粹的人造亡灵也有了欲.望，会怎么样？他们会因单纯的**而强大吗，那么，首先，就从‘暴食’所代表的食欲开始吧。”

    我原来是打算仿造七宗罪，做了七次样本试验，但可惜七宗罪并没有全部完成，也没有进入量产，倒不是因为试验结果太差，而是结果实在太好。

    第一个“食肉者”阿当，七宗罪的长子，就已经是整个亡灵史上最恐怖的怪物了，而随着我亡灵魔法的长进，后面几个原罪越发可怕，甚至，都有无法控制的趋势了。

    于是，估量到这样下去很有可能会重蹈覆辙，迟早结合两个世界的知识制造出毁灭一切的怪物，我就停手了。

    “哼，我承认我对元素魔法没什么天赋，魔力共鸣之类的更是完全学不会，但亡灵魔法一学就会，造物天赋强的我自己都怕呀，果然，疯狂科学家之类的，天生就适合毁灭世界呀。”

    难得的梦到往昔，而刚刚从沉睡中苏醒，我就被楼下的声响吸引住了。

    往日城管们吵闹的出操声和整齐的口号声都不见踪影，看来，是都按照我的命令上街执法了，但往日她们练习的平地上，却依旧有人正在对练。

    不大的空地上已经满是寒冰和火焰的痕迹，只是这次交锋的主角，却不仅仅是伊丽莎和安妮了。

    “寒冰造型;连弩炮阵！”

    随着伊丽莎一声令下，背后的数十个寒冰弩车全部弹射，打着旋的冰之弩箭，全部集中攒射半空中那个目标。

    而持剑的安妮，则也召唤了火焰双翼，如怒焰天使般徘徊下击，努力缠着对手。

    可惜的是，即使她借着俯冲之力冲撞之下，对方也只是伸出一个手指轻轻一挡，那剑身就歪倒了一边。

    力量的差距实在太大，神剑连皮都擦不破，这只是最少两个职介的力量差距。

    而此时，冰之弩矢已经到了小红的眼前，她却仿若没有看到，只是摇了摇头，似乎对小辈的表现很不满。

    “哈！”一声大喝，安妮和伊丽莎的动作就突然停滞，而随着，直接被气浪冲的头昏目眩。

    “太慢，太弱，太无力。你们元素提炼不纯，肉搏技巧不精，进攻脚步松散，躲避反应迟钝，没一个动作像样的，就你还想跟我同台较量吗？做你的美梦”

    终于，小红似乎恼怒了，长腿使劲一跺，整个地面来回激荡，莫名的震波之下，还没有恢复平衡的伊丽莎和安妮站都站不稳，就直接倒下了。

    而到了眼前的冰之弩，小红只是瞪了眼，就直接在空中解体，连她的肌肤都无法触及。

    好吧，第三次比试，或者说单方面碾压，持续时间不到两分钟，已经在此结束。

    “看来，还真是被刺激到了。”

    那天，谈判桌那边是二十几个传奇加几个圣阶，而这边，才是两个年少的黄金，同样是未来的城主，差距如此巨大，就算再这么豁达，也会被这巨大的实力差距刺激到了。

    这也是我让她们自己去谈判的目的，残酷的现实也是最直接的动力，若两人真能够就此突破，手上再多两个传奇，这场从一开始就不对称的牌局就会好打很多了。

    “窗台上看戏的，既然看到了，不下来指点一下。你们人形生物的作战方式我实在不习惯，这个状态我连一半作战能力都无法发挥，你们的境界提升更是让人莫名其妙，还是你来教吧。”

    而耳边小红的督促，却让我无法再安闲看戏，小红干架是一把好手，战斗经验何其丰富，传授实战经验效果明显，但教人的话……龙族成长九成靠天赋，自己都没经历过修行提升，怎么当人师傅。

    她教你双翼拍击、爪牙合击、神龙扫尾、高空俯冲龙息，你学的会吗？恩？貌似伊丽莎还真可以学一点，至少神龙扫尾可以学学，但想起越来神出鬼没、越来越凶残的半恶魔女仆，我还是决定不搬起石头砸自己的脚了。

    实际上两个黄金巅峰的问题我已经很清晰了，她们的基础早已达标，剩下的只是凝结灵魂徽印了，而那个徽印是凡人突破传奇领域的结晶，毕生经验和力量的汇聚，步入更高境界的基石，简单的说，她们要确定自己未来的发展方向了。

    但到了具体的，却是完全不同了，伊丽莎是道路太多，不知道走那条。

    “伊丽莎，种族：深渊眷顾的半只恶魔，力：22敏：20体：20智：24魅：18意志：5，职业：60级法师/1级法咒使/12级寒冰行者/10级圣拳士（专精消灭亡灵的武僧进价）/5级刺客总等级：88（由于没有灵魂徽印，综合等级评价79黄金巅峰）”

    百年岁月，她学的太杂，我传授的寒冰魔法、来自深渊的半恶魔天赋能力、玛格丽特教授的常规魔法、自创的线魔法，还有不知道在哪里学的盗贼技巧和拳师技巧（看到有亡灵杀手之称的圣拳士居然有10个等级的时候，我脚跟都在发软），在基础能力和属性达标后，任何一条道路，都可以让其跨入传奇.......但有时，太多选择，反而让人无法下定决心，选择一项，等于放弃其他所有，她还在犹豫。

    而安妮则是基础太薄了，凤之传承让她实力飙升的速度超过了经验累积速度，找出适合自己且潜力无穷的道路，对还不到二十的她的确是件难事。

    这种涉及到未来发展路线的选择，外人是很难帮忙的，毕竟，最适合自己的道路只有自己知道，这也是小红为难的原因，她没法替两人做出人生决定。

    但对我来说，却不是全无办法的，我们不能帮她们做出选择，但却可以提供别人做出选择的经验，让她们参考，其他人的道路和经验，足以成为她们成熟的催化剂。

    按理说，进入传奇四次凝结了四个灵魂徽印的我，应该最有发言权，但实际上，除了代表律法之力的公正之印，其他三个都是用系统这个作弊器砸经验和点数换的.......而公正法印更来的出奇，相当于发明律法之力后被秩序之源给的附赠品，更无法作为案例和经验。

    “小红，和她们说下亚当和玛格丽特冲击传奇的经历吧，应该有帮助。”

    “喂，你是当事人，为什么不亲自说嘛？”

    “我忘了，也懒着翻日记了。”为了防止死亡对于记忆的遗失，我一直保有记录日记的习惯，如今的日记本已经堆满了一个地下图书馆，而这明显太早的事情，查起来也太麻烦了。

    “.....好吧，好吧，我说。”幻音术传来的嗓音，却有些莫名的情绪低落。

    摇了摇头，提振下精神，小红拍手道，

    “........玛格丽特突破传奇的经历比较奇特，可能也无法做出参考，当年我们在流苏精灵王国，好不容易办了国立图书馆的借书证，只能使用三天，她在里面看书看的不想出来，结果三天居然看完了整个图书馆的藏书。而且，出来的时候就已经是传奇了，她凝结时光回廊的缘由，只是单纯想多看点书。”

    她的声音并不小，不仅在平地的其他强者也听到了，甚至，连在二楼我的都听的很清晰。

    “既然忘记了，就再记一次吧。除了找自己所需要魔法资料、莉莎要的藏宝记录、亚当要的武技资料、我找的神器资料外，她还在里面为你查了精灵王国关于岚之国覆灭的全部记载，你当时说过，你欠了她一个很大的人情，有机会，记得还吧。”

    这次，小红的耳语只在身边响起，我却愣住了，岚之国是我过去已覆灭的国度，当时冒险的一个主要目标就是查清它被背叛乃至覆灭的来龙去脉，没想到，居然会是从哪里得知的，看来，失忆的人永远不会知道自己到底忘记了什么重要的记忆，死亡还真带走了不少东西。

    而当我认真聆听的时候，小红却哑口失笑了。

    “至于亚当，噗，那还真是个笑话。”

    听到威名赫赫的城主的名字，听众们更有兴趣了，谁会不想知道那位人间史诗英雄的过往，还在营地的众人大多都在努力伸直耳朵偷听。

    “当时呀，我们冒险小队得罪了一个信奉邪教的混蛋领主，结果，他派人缠住了我们的圣骑士队长罗罗和战士亚当，然后袭击了我们的临时驻地，抓走了当时在养伤的法师玛格丽特、盗贼莉莎，还留了个条子扬言，若三天内不到某某地某某府自投罗网，就要XX人质，先X后杀.....”

    “然后亚当大人就怒气爆发了吗？一下子跻身传奇然后来了个英雄救美，虽然路线有些俗套，但的确很经典呀。搞不好，玛格丽特大人就是那次喜欢上他的。”快嘴的莫莫和戴安娜是唯二没有上街“休闲”的城管，此刻，两人也加入旁听之中。

    “呵，当时，亚当的确气爆了，持剑向天，怒吼震天‘卑鄙无耻的小人，我一定要把你碎尸万段，就如此纸’，然后潇洒的把信丢到空中，快剑连闪，斩成了粉末，然后任由碎末被风吹去，潇洒无比.....”

    “然后就是遇到强敌战斗，然后就到传奇了吗？”

    “不，接下来一句是‘糟了，我还没有记下来地址！！怎么办？？？’........”

    “啥！！！？”

    说到这里，小红满意的收获了所有人惊诧乃至痴呆的眼神。

    “等下，那怎么办？玛格丽特大人和那个女盗贼怎么了？”

    “不至于吧，怎么能傻到那种程度。那后来怎么办。”

    摇了摇头，带着回忆的表情，小红继续说道。

    “是的，没有地址，等于失去了救人的线索。小队的队长罗罗气死了，一巴掌就把亚当那个蠢货扇的四米远，狠狠的撞到树上，那还是我们第一次见到对谁都温和有礼的罗罗大哥气成那样。然后，亚当那傻子就直接疯一样的出去，到处寻找.......”

    “然后找到了？在三天内找到了?”事关自己被誉为完美英雄的养父，安妮也有些好奇。

    “不，他第五天才找到。”

    “那.......”

    这下，所有人都吓到了，谁都没有料到亚当城主居然还有这样的黑历史，那不是说两个人质已经遭到了......

    小红却又摇了摇头，笑道。

    “你们想到哪里去了，那是的亚当还不成熟，相当不靠谱，但不代表罗罗大哥也不靠谱呀。他花了二天一夜，从湖里，从草丛中，硬是把四千六百五十四条碎片....或许，应该叫碎末了，全部找齐，然后一点一点的拼了起来，硬是找到了地址，然后连夜赶路，累死了两匹战马，及时赶到救下了玛格丽特和莉莎。之后过了很久，亚当那小子才顺着绑架者留下的痕迹找过来。”

    “......这也太不可靠了吧。”

    “的确，看到玛格丽特她们完好无损的时候，他大哭了一场，哭的像个孩子。之后，吸取了教训，就靠谱了很多，至少懂得了谋定而后动。好了，故事完，姑娘们，有体会了吗。”

    若有所思的两人点了点头，却同时惊叫出声。

    “没想到义父/城主大人居然还有这样的黑历史......等下，你还没有说他是怎么突破传奇的。”

    “呵呵，当年呀，他一边抱着大家哭，一边发誓说永远不会放弃同伴，以后一定要把大家保护的好好的，就凝结了自己的‘不灭之凤’，怎么都打不死，成了真正的蟑螂命。”

    此刻，小红脸色却带着了笑。

    “大概，这次，还真是玛格丽特喜欢上亚当的契机了吧，毕竟，每个女孩都对一个可靠的保护者有期望。”

    “不。”否定的答案却出于不是当事人的安妮的口。

    “我问过玛格丽特姐姐为什么会喜欢义父，结果玛格丽特姐姐当时苦笑了半会，才只说了一句‘当时哭的太可怜了，觉得这样放任他不管，他早晚弄死自己。’我之后来追问，她却不肯和我说了，那个‘哭的太狠’，大概就是指的这一次吧。”

    “......觉得无法放任不管，就总是把眼睛盯在他身上，然后就不知不觉的沦陷了吗？其实，也很浪漫呀。”

    戴安娜点了点头，却发现所有人惊诧的看着她。

    “怎么？我说的不对吗？”

    “不。只是没有想到暗精灵也会有正常的恋爱观。”虽然这明显是大家的共识，却没有人说出口，反而只是集体摇头。

    “......这不公平。”一个冰冷的反对声音，却在墙边响起，那是沉默了半饷的伊丽莎。

    “怎么不公平？”

    “明明能够得救，是那个罗罗花了那么多精力和努力才做到的，为什么玛格丽特大人却会感谢帮倒忙的亚当，甚至还喜欢上她。明明那个罗罗才是应该被感谢的呀。他拼齐了纸条，赶路救下了同伴，却得不到应得的感激，好不公平。”

    小红楞了，满脸诧然的看了伊丽莎半天，才挤出一句。

    “当年，莉莎也是这么说的。然后，她缠上了了罗罗，罗罗却始终没有答应。”

    “那么，那个莉莎大人和罗罗大人现在怎么样了？”

    “罗罗吗？失踪了，留下一封信，说自己找到了仇人，要去复仇，然后，谁也不知道他去哪了。不过，后来找到了他被亵渎的尸体，可能已经被转化成不死生物了吧。至于莉莎......她死了。”

    “死了？”

    这下，小红看伊丽莎的眼神就很奇怪了。

    “是的，死于永夜大君掀起的亡灵天灾，死在当时永夜大君麾下四天王之一的骨龙女王格蕾.丝之手。”

    伊丽莎眉毛一挑，她不知道罗罗和亚当的往事，但她却知道永夜大君是谁，她想说什么，但既然当事人都没有再计较，她也没有插嘴的余地。

    “您这样看着我，是有事要吩咐吗？”

    “不，我只是觉得，有时候记性不好，也未必不是好事。”

    －－－－－－－－－－－－

    “阿......嚏。”

    “格蕾.丝姐姐，你也感冒了吗？”

    “是歌莉娅，歌莉娅。亡灵怎么能够感冒，大概和你一样，被人念叨了吧。嗯，或许，是大帝想念我了！快点，阿当，就快到了。”







  



