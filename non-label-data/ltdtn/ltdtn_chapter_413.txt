    亥时。
    郭北县万家灯火已经熄灭了，静悄悄的街道上只有打更的人偶尔路过，便是夜里当值的差人也偷懒不知道溜到了哪里去。
    李家，李府之上却还是灯火通明。
    “李管家，这次我来是为了那些镖师的事情，郭北县内的镖师来了多少了？”李修远问道。
    李管家候在一旁毕恭毕敬道：“回大少爷，以及有七千余人了，而且这几日还在增加，马上就要过年了，镖行的生意还算红火，大少爷一时间抽调了这么多人一些镖头可有不少的抱怨。”
    “这是难免的，不过要吃我们李家这碗饭就得守着我们李家的规矩，抱怨两句没什么，如果有人借此生乱的话，用不着客气，该打发的打发，该教训的教训。”
    李修远道：“这世道你也知道，不太平了，镖局这门生意在太平盛世难做，碰到年经不好，或者是朝廷腐败的时候，不知道多少汉子愿意提着脑袋来吃这碗饭。”
    “大少爷说的很对，尤其是今年愿意加入镖局的汉子不少，县里的镖局已经扩了又扩，现在都搬到县外去了，现在京城那边的镖局也是遍地开花。”李管家点头表示赞同。
    “镖局开到京城不算什么，我要镖局开到这天下任何一个县里去。”李修远道：“对了，那七千余人现在在哪带着？”
    李管家道；“在谷场那里待着呢，那地方大，老爷临时建了一片屋子，大少爷，这召集这么多人是做什么啊，几千号人，这新来的县老爷都吓到了呢，还亲自登门拜访询问老爷怎么回事呢。”
    李修远笑了笑：“这县老爷比之前那个刘县令安分多了，这回见到了我们李家的底蕴更加不不敢怎么样了，这事情我也不瞒你，这次召集这群镖师是想把他们整编整编，弄支军队出来。”
    “弄军队？大少爷这是要造反么？”李管家一惊。
    “李管家别说你不知道，李梁金在扬州造反的事情，他的军队都要打到金陵城来了，离郭北县也没多远了，再不提前做好一些准备，贼军来了，李家这偌大的家业和一家老小的性命全得送人了，我已经托人去京城买官职了，只要官职一到，我这就算是招募乡勇，组建游击军，不算逾越，更加算不上是造反了。”李修远笑道。
    “李梁金造反的事情老奴也听镖局的人说过，现在势头很大，十几个镖局的人撤出来了，有些撤不及的也被裹挟进了叛军之中。”李管家道。
    “此事父亲知道否？”李修远道。
    “老爷是知道的。”
    “那就好，兵器铠甲什么的也早早的开始打造了吧？按照我以前留下来的样式，半身板甲，破甲长枪，腰刀，弓弩。”李修远道。
    他十几年的时间可不是都在下河村跟着自己师傅练武，他早就在做着这些准备，连兵器铠甲都已经准备好了样式，一旦情况有变就可以立刻用灌钢法快速的打造出来。
    便连打造兵器的钢料也每年，每个月，乃至于每天都有炼制，都暗地里屯放着呢。
    因为李家名下的铁矿就有好几座，总不能闲在那里吧。
    这是李修远这个穿越之人的危机意识，因为没有长盛不衰的王朝，万一李家碰到王朝更替的那一段时间，这些东西能救命呢。
    纵然是自己不用，也能留给后代当做晋升之资，献给某个王侯，这是一个家族的生存之道。
    李管家说道：“收到大少爷的信件之后便在开始赶制了。”
    “这就好，马场那边现在有多少健马了？”李修远道。
    开铁矿，囤钢料这么危险的事情都做了，养马自然更是不在话下。
    而且历朝历代都是鼓励养马的，养好了马还会得到朝廷的褒奖，这样光明正大又能增加李家底蕴的事情李修远怎么可能不做。
    “除去种马和一些生育的母马，已经岁数不到的小马之外，马场那边应该有健马一万三千匹左右。”李管家道。
    李修远皱起了眉头：“我不是让马场一直保持两万匹健马的数量么？”
    “老爷上个月卖了一批，大约有五千匹，一匹卖得五十两，赚的白银十五万两，那买家出手阔绰，而且又是付的现银，开价这么高老爷也就做了这笔生意，若非大少爷以前说马场至少留两万匹健马的话，老爷估计会把剩下的一万三千匹也卖掉。”李管家道。
    “......”李修远嘴角一抽。
    上个月卖的？
    这买马的人是谁他已经多少猜得出来的，不用说，一定是那李梁金。
    李梁金虽然是这个月造反起事，但肯定上个月就在准备了，这个小侯爷真是有钱啊，十五万两买马，够有决心的，还好自己父亲没有把马全卖给李梁金，不然的话这仗没得打了。
    打仗装备之中最重要的就是马匹，没有马匹寸步难行。
    更别说现在有鬼神夹杂其中了，李修远输人不输阵，这装备是断然不能比李梁金要差的。
    李管家道：“大少爷不用担心，等明年秋天，这个缺口大致能补上。”
    “算了，卖就卖了吧，别人用真金白银买的，又不是抢去的，也不能怪父亲。”李修远道。
    “还是公子深谋远虑，早年家中积蓄全拿去开马场，作坊，买矿山了，现在这几年碰到粮食涨价，盐铁涨价，健马更是越发稀少，已经赚了不知道多少倍了，老奴在李家大半辈子，从以前一桩生意几千两银子，便算大的了，到现在一桩大生意能到十几万两银子，真是万万没想到变化会这般大啊。”李管家奉承的笑道。
    李修远摇头一笑，这还需要深谋远虑？
    王朝更替不过两三百年，本朝已经建国快三百年了，算算时间也知道差不多了，准备这些东西只有赚不会亏。
    “今日很晚了，有些事情明日再和管家商议吧，管家早些去休息，你年纪大了可熬不住夜，明日陪我去谷场走一样。”
    李修远看了看天色，已经很晚了，也不好继续拉着管家问话，就让他去休息去了。
    “是，大少爷，那老奴就先去休息了，大少爷也早些休息。”李管家道。
    目送管家离开之后，李修远也回自己的院子准备休息。
    “嗯？怎么还有灯光？”
    他来到院子，看见屋内居然还有灯光，不由有些疑惑起来。
    当他推门而入的时候却看家杜春花撑着脑袋靠坐在床边，似乎在守着他回来。
    “少，少爷，你回来了？”
    开门的声音惊醒了她，杜春花迷迷糊糊醒来见到李修远走进来当即欣喜的站了起来。
    “春花，还没睡么？”李修远点了点头。
    “没，我听到少爷你回府了便没想去睡，大少爷你等着，我这就给大少爷去打热水洗脸。”
    杜春花之前的睡意荡然无存，见到李修远时仿佛浑身上下都有了劲。
    “小蝶呢，她睡了么？”李修远问道。
    杜春花很快打着热水，端着铜盆走了进来，刚从外面回来的她忍不住打了一个寒颤，然后道；“小蝶早就睡了，她熬不住夜。”
    说着又利索的洗了汗巾，然后递了过去。
    “外面冷，不用候着我，你应该先睡的。”
    李修远接过汗巾却发现杜春花的双手冰凉，怕是一直在等自己等凉了。
    “没事，我不冷，以前在村子里的时候比这还冷的天我都要去洗衣裳呢。”杜春花笑着说道。
    李修远洗了把脸，然后却是伸手挥了挥：“过来。”
    杜春花眨了眨眼睛走了过来。
    李修远伸手将其涌入怀中，感受到她的体温，然后道：“还说不冷，身子都凉了，再等下去怕事要生病了，以前你在村子里的时候是没男人照顾，现在你跟了我难道还要挨冻受苦么？以前在道观的时候我不是说了要照顾你么，难道你忘记了？不过比起前阵子，你最近又白皙，丰腴了不少，有美人的胚子了。”
    “哪，哪有，我还不是和以前一样么？”
    杜春花被这一夸，那白皙的脸蛋顿时一红，她却是不抗拒任由男人抱着，感受到那阵阵温热传来，她感觉浑身的寒意都被驱散了。
    “少爷身上真暖和，像是火炉一样。”
    李修远笑道：“这是当然，我是习武之人，气血旺盛，自然不惧这寒冬，好了，夜深了，去睡吧。”
    “那，那我今夜睡在哪？”杜春花红着脸细声问道。
    “你想睡哪？”
    “当然是睡在少爷身边了。”杜春花抬起头，忍着万分的羞意说道。
    李修远捏着她的俏脸笑道：“怎么，想我了？”
    “嗯......想的紧。”杜春花脑袋埋在男人怀中，细细的应了声。
    “是我太忙了，冷落了你，休息去吧。”李修远叹了口道，只是将这女子横抱了起来，然后往回走去。
    杜春花顿时娇羞一片，偷偷的看了一眼这个英俊不凡的男子，芳心跳的急，待男人低头看一眼的时候她又急忙闭起了眼睛，脑袋埋进了男人怀中。
    很快，卧房内的灯火一灭，一切都陷入了黑暗之中。
