    吴象被封为神力将军，并没有引起百官们太大的反应。
    他们只是试着斩掉李修远的左膀右臂罢了，若是不成，也没关系，毕竟不损失什么。
    用李修远之前的话来说，就是分寸。
    吴象能否得到封赏并不重要，他们掌握了这个分寸，岂能不拿捏一番？
    现在朝廷之上的李修远只是在被动的防御而已，只要一下没有防御住，就会立刻被穷极猛追不舍，到时候会有什么样糟糕的结果都意外。
    “文人杀人不用刀啊，我带着镖局的上万汉子拼命平息了九山王之乱，这些文官三言两语就想把这份功劳抹去，若是让他们得逞，我那两千多属下的性命岂不是白白牺牲了么？而且他们也不想一下，我若不
    去平乱，一旦李梁金成了气候，这大宋国江山社稷都要动摇。”李修远眼睛微微一眯。
    真是让人寒心啊。
    他现在心中对于这些文官充满了厌恶。
    君子有恩报恩，有仇报仇，李修远向来不是弱善的之人，找到机会他是不会放过这些官员的。
    此刻，赵官家封赏了吴象之后，又看向了跪在地上的韩猛。
    “你叫韩猛是么？听李爱卿说你曾带一对人马追杀李梁金，把那贼首逼入绝境。”赵官家笑着说道：“真有将帅之才啊，朕能得尔等这样的文武才俊，心中甚是喜悦啊，你想要什么样的封赏？不妨和朕说说
    ，朕会酌情考虑的。”
    韩猛楞了一下，心中有些狂喜，没想到赵官家态度对自己如此友好，丝毫没有皇帝的架子。
    可是随后他却又心中一凛，想到了之前大少爷叮嘱的话，不由平复了一下内心激动的心情，然后道：“小人只是一介武夫，以前听读书人说，雷霆雨露皆是君恩，小人不敢奢求什么，官家赏赐什么都是小人
    的荣幸。”
    赵官家微笑着点了点头，然后道：“你名叫韩猛，有猛兽之意，戾气重。朕觉得颇有些不妥，朕就赐个名给你吧，以后你改姓赵，名叫世忠如何？希望你世世代代忠于朝廷，忠于朕。”
    收买人心？
    李修远脸色一凝，有些异色的看着这个面庞有些浮肿，无精打采的赵官家。
    没想到这个赵官家会来这么一手，改姓赐名。
    改姓赐名这是有寓意的，就是在告诉韩猛，让你改投门户，不要给李修远效力，转而给皇上效力。
    若是韩猛答应的话，李修远相信即将到来的封赏绝对不会低。
    但若是韩猛答应的话就意味着背叛李修远，而这不管成功与否，都等同让李修远自断一臂，因为只要韩猛不同意......下一刻那些文官就会像是野狗一样扑上来，弹劾，问罪。
    而赵官家到时候自然也乐意顺应大臣的意思，把韩猛给弄掉。
    “帝王心术么？”
    李修远目光闪动：“还以为这赵官家是一个昏君，现在看来这皇帝虽然有昏庸的时候，但是也不蠢，皇帝该有权谋心术一样没有落下，这一手是明摆着削弱我的势力，看来也是对我起了防备之心，看来李林
    甫说的很对，我此番京城之行不可能顺利。”
    随后他也理解为什么赵官家现在就开始迫不及待的对自己打压了。
    很简单，因为李修远年轻。
    如此年纪轻轻就坐拥一州之地，麾下兵马近五万，还有一万骑兵，以现在朝廷的实力想要桎梏这样一位新起之秀是很难的。
    赵官家这一任皇帝若是不开始动手的话，等到下一位皇帝登基，李修远只怕在江南称王的实力都有了。
    若是再熬一代皇帝。
    这天下是赵，还是李都难说。
    所以赵官家这是在未雨绸缪，他看到了李修远潜在的巨大威胁。
    李修远此刻想通之后也在脑海之中快速思索起来，如何应对这个情况。
    韩猛没有如此高的智慧，他虽然老练可是见识太少，还不知道赵官家的险恶有心，所以一时间楞了一下，不知道为什么当今皇上为什么要给自己改姓赐名。
    “这，这应还是不应呢？”
    韩猛心中没有谱，可是看见大少爷那神色凝重的样子就知道这不是一件好事。
    因为大少爷但凡遇到不如意的事情就会皱眉，凝目，这是一个下意思的举措，跟在身边的他多年早已经知道了。
    “哦，难不成你对朕的这个封赏不满意么？”赵官家又笑着开口道，神情没有半分威严，就像是一个老好人一样。
    李修远见到韩猛摇摆不定，脸上急出了冷汗，便知道自己不能不管不问了，他立刻传音入耳道：“韩猛，应下来，但只能改名不换姓，若是皇帝问起就以孝道推了。”
    这传音入耳，不过不是法术，是习武之人的手段。
    这声音并非听不清，只是一旁的人听的只是嗡嗡作响，像是什么蜜蜂在震动一样。
    但是在韩猛的耳旁，这嗡嗡的声音却是刚才说的一句话。
    “震动劲气，传音入密，这李大人竟是武道宗师？天啊，这这么可能。”
    还未离开的执戟士听到那嗡嗡的声音眸子一缩。
    有几人甚至下意识的握紧了手中的方天画戟，感到一阵后怕，比之前吴象举起铜狮子还要让他们吃惊。
    因为李修远完全是一副文官的样子，丰神俊朗，气质颇显儒雅，丝毫没有一位武人的样子，但偏偏就是这样一个人，竟然能做到武道宗师才能有的传音入密。
    该不该提醒呢？
    和十位执戟士心思各异，有执戟士想要提醒官家。
    但是想了想，却又忍住了。
    似乎没有必要自讨不快，而且也没有规定文人就不能有武艺啊。
    此刻，韩猛得了李修远的提醒，心中大为松口气，伏地跪拜道：“小人多谢官家赐名，但还请官家准许小人不改姓氏。”
    “为何？”赵官家问道。
    “父母姓，不敢忘。”韩猛道。
    赵官家楞了一下，便是其他的文武百官也楞了一下。
    这个韩猛不一般啊......竟然能这么快想到应对办法。
    “呵呵，原来如此，朕准许了，大宋国以孝治国，你有此孝心难能可贵，朕就赐你世忠之名，封你为扬州总兵，希望你今后好好为朝廷效力。”赵官家道。
    “韩世忠拜谢官家。”韩猛道。
    “韩世忠么......”李修远听到这个名字不知道为什么微微一怔。
    不过至此。
    吴象，韩猛两个人的封赏算是保下来了。
    一个得了神力将军的一位杂号将军的封赏，一位得了扬州总兵的封赏。
    但谁能想到这朝会封赏本来是一件大好事，可其中的凶险却是外人所不能体会的。
    可以说李修远和赵官家，还有这些文官斗智斗勇。
    而后，赵官家忽的开口问道：“朕有一事，心中疑惑。李爱卿，上元京城大火那日可是你出现在皇城之下，登台求雨？”
    “臣那日的确登台求雨过，对郭真真人多有打搅，让官家见笑了。”李修远道、
    赵官家感慨道：“却是有心了，李爱卿你对那场大火怎么看？”
    “京城每年都有火灾，这次应当是一个意外吧。”李修远想了一下说道。
    “意外？哼，你这样的回答如此有能力管理好扬州？下官认为一定是五通教的妖人作乱，而且就在昨日，京城知府方生余，方大人被妖人刺于府上，凶手不明，六扇门的人虽然正在追查，但却从其妻子的嘴
    中得知了五通教的事情，可见这五通教妖人已经胆大包天到这地步了，朝廷不能再坐视不管了，应当立刻剿灭五通教，还京城一个安宁。”一位文官立刻道。
    “老臣附议，不过老臣举荐李刺史负责此案，李刺史有平灭白莲教，弥勒教这些妖人的功绩，由李刺史负责上元节大火一案和方生余被刺一案最合适不过。”
    一直不动的中书省杨大人此刻蓦地站出来开口道。
    “儿臣不同意，李大人初来乍到，对京城丝毫不了解，并不适合负责此案。”三皇子赵景站出来道，他见势不妙觉得有必要援助李修远一把。
    “三皇子，此言差矣。”
    吏部侍郎张元却又站出来笑道：“一州刺史官职何其重要？管理一州百姓，既要负责春耕秋收的农事，也要查办各府，县的大小案件，还要收拢流民，战后重建.....非有能力之辈不得担任，李刺史年纪轻
    轻，经验不足，若是连一个小小的案件都处理不了，如何有能力担任刺史职位？”
    “若是李刺史能顺利的破获此案，官家才能放心让李刺史管辖扬州，如此一来既能给京城百姓一个交代，也能堵住天下读书人之口，岂不美哉？”
    “美哉你老母。”李修远听的差点忍不住骂人了。
    这些文官真够狠的，自己扬州刺史好好的一下子变成了京城知府，这哪里是封赏，分明就成了惩罚。
    而且这事情办好了也顶多顺利的回到扬州去，若是办不好......只怕这些文官是不会轻易的放过这个机会的。
    李修远正欲想办法推掉，然而这个时候却又一个熟悉的声音响起。
    “罪臣也赞同杨大人的提议，李刺史过于年轻，主政一方还颇为稚嫩，上元节大火和方生余被刺一案让李刺史负责最合适不过，若是他连这能力都没有的话，罪臣建议官家收回扬州刺史的封赏，切不可让稚
    子误国。”
    此刻却见，傅天仇从大殿外走了进来。
    真不知道他是什么时候从天牢放出来了，还参加了这次的朝会。
    “......”
    李修远嘴角一抽：“老丈人你这是在故意坑我是吧？是不是因为我之前在金陵城的时候给了你脸色。现在给我玩这一套？”
