    靠近京城的一处小道上传来了打斗厮杀的声音。
    是一伙强盗在截杀路过的旅人。
    “他娘的，从来都是老子截杀别人，今日真是晦气，竟被别人截杀，这群强盗邪门了，哪收到的风声知道我们身上有钱银，竟舍得派几十号人截杀我们，难道不知道这是一门亏本的买卖么？”一个光头汉子手持一柄百炼钢刀，恶狠狠的盯着附近的强盗。
    一位强盗冲来，他抬刀就砍，那强盗惨叫一声倒在地上请客之间就没了气息。
    “吴非，别顾着和他们打斗，夺马我们冲出去，贼人势强，我们不是对手的。”马东大声喊道，他亦是和牛二一起持刀搏杀。
    三个人武艺高强，护住了中间的朱昱，在一群强盗的围攻下竟然坚持了下来。
    在旁边，他们从金陵城带来的三匹健马已经死了两匹，是被箭射死的，强盗早就埋伏好了在这里，在几人休息的时候出手偷袭的。
    饶是如此，这群强盗亦是没有得手。
    朱昱看见这般惨烈的厮杀，瑟瑟发抖，躲在了马东和牛二的身后，生怕被乱刀砍中，嘴中不断的叨念起来：“我不能死，我不能死，我不能死......”
    他知道自己绝对不能被截杀在这里。
    自己若是死了，谁去替李修远买官？扬州的动乱怎么去平。
    李修远放弃了大好的前途，放弃了所有的功名利禄，宁愿散尽家财，入武职平乱，自己又怎么能死在这里呢？
    便是要死，也要完成了此次大事再死。
    “老子当然知道冲杀出去，可是这么多人怎么冲？这些强盗连马都没有我们夺马的机会都没有，这样下去我们一个也走不到，必须要有人能留下来断后拖住这些贼匪，让另外一个人带着朱昱冲出去，我们剩下的马只有一匹，想要全走是不可能的。”吴非咬牙道。
    马东当即喝到：“我和牛二留下来断后，你护着朱昱去京城。”
    吴非杀退一个贼人，退后几步说道；“想清楚了，断后是会死的。”
    “死便死，大少爷不是一直说生当为人杰，死亦为鬼雄么？铁山敢护着大少爷死在那黑虎嘴下，今日我们二人为何不敢为大少爷战死？现在正是我们以死报恩的时候，吴非，你的武艺比我们要好，你护着朱昱冲出去的机会要大，快走。”牛二亦是喝到。
    “他娘的，你们先撑住，我送走朱昱之后立刻回头救你们。”吴非一咬牙，当即抓起旁边的朱昱，便夺马奔走。
    附近的强盗围杀过来，马东和牛二拼死抵挡，硬生生的抗住了这些贼匪的进攻，身上被砍上数刀而不后退一步，他们知道此次任务至关重要，必须让朱昱活着去京城，
    吴非骑着健马护着朱昱冲了出去，丝毫不管身后的两位弟兄。
    他知道自己不能有一丝一毫的犹豫，不然自己也会被留下来。
    就在这里厮杀惨烈的时候，附近的山丘外，一股阴风呼啸，天空之中一片乌云欲飞过这座山丘可是却被这山丘给拦住了。
    小小的一座山丘，竟拦住了天上的乌云。
    乌云之中，阴风呼啸，人影，兵卒若隐若现，不是活人，皆是鬼神，此刻持剑鬼王手持双剑带着麾下几百阴兵隐藏在乌云之中。
    持剑鬼王怒喝道；“我乃人间圣人麾下的持剑鬼王，你们两尊山神为何要拦我等去路？”
    他看着不远处那场厮杀，气的连连暴怒，这可是人间圣人身边的护卫，而且护着朱昱是要去京城办重要事情的，若是这里出了差错，那人间圣人一怒岂能得了。
    然而他准备去施展法术帮忙的时候却被眼前的两尊山神拦住了，
    一尊山神身披华丽的白甲，带着头盔，面具看不出相貌，另外一尊山神身披黄甲，带着金色的面具同样看不清相貌。
    “放肆，你是南方的鬼王，让你这样的鬼神入京岂不是要惊扰到圣上的安危？我等乃是大宋皇帝御笔亲封的寿山山神和艮岳山神，职责便是护卫京城，管你是人间圣人麾下还是天宫之中天宫麾下的鬼神，没有陛下的圣旨特招，任何鬼神都休想进入京城。”
    那白甲的寿山山神大声喝到，声音滚滚如雷，震的附近狂风大作。
    他们是皇朝敕封的山神，身前都是忠心耿耿的将军，死后亦是护卫国都的安全，不管是人间圣人也好，还是天宫，阴间派来的鬼神也罢，只要没有圣旨，口谕都不会被放进京城里来。
    “你们这两尊山神当真愚蠢不可救药，人间圣人降世便是济世救民，你们却为了什么京城的安全拦住本王，难道不知道这事情的后果么？莫以为人间圣人柔弱好欺，他若是知晓你们这两尊山神坏了事情定不会饶恕你们的、”
    持剑鬼王怒瞪这两尊山神：“还不让开道路?”
    说完他便再次带着阴兵冲了过去。
    “无视朝廷的威严，便是鬼王也当诛杀，人间圣人算什么，比的上当今的陛下么？”
    艮岳山神大喝一声，手持一柄巨剑便杀了过来，抬剑便砍。
    他的剑足有三丈之长，上面刻着两个大字：镇山。
    这是当初他们被敕封山神，大宋皇帝特意命人打造的镇山剑，上面还有朝廷的金印，敕文，代表着王朝的威严，可震杀一切牛鬼蛇神，只有他们这种被朝廷敕封的鬼神才有资格使用。
    一剑砍下来，持剑鬼王急忙招架，可是手中的双剑像是纸糊的一样被这三丈之长的大剑砍中立刻就咔嚓一声当场断裂，而后大剑劈进了他的身体之中，将他鬼王的躯体都直接砍成了两节。
    半片身躯直接化作一股阴气和香火当场炸开，剩下的半片身躯重新凝聚，再次化作了持剑鬼王。
    “好厉害的法宝，我不是他们的对手，快走。”持剑鬼王却是大惊失色，急忙卷起身后的阴兵便迅速后退。
    连一招都接不下来，他即便是有心效忠，也无能为力啊。
    若是两尊山神一起出手，他是必死无疑的。
    “算你这鬼王逃的快。”
    寿山山神和艮岳山神见到鬼王溜走，也不去追，他们的任务只是护卫京城的安危，其他的事情和他无关。
    见到鬼王留下的那一团阴气和香火，两尊山神又是张嘴一吸，吞入腹中，然后打了一个饱嗝，似乎已经是吃饱了，身体也变的比之前更加的健硕和强壮。
    他们也是鬼神，这些对于他们来说是补品。
    且不闻钟馗吃恶鬼的故事？
    为何要吃恶鬼，除了消灭恶鬼之外也是在增加自己的道行。
    不过吃鬼神增加道行是不被容忍的事情，天宫不会允许，阴间的阎罗也不会允许，不过眼下天宫阴间都混乱不堪，这两尊山神仗着朝廷的存在却是一点也不顾忌。
    越不过这两尊山神的守护，受伤的持剑鬼王只能带着阴兵逗留子在京城的地界之外，没办法再护送马东，牛二他们几个人了。
    “朱昱，老子就送你到这里了，这里是官道，路上都有来往京城的旅人，不可能再有强盗了，只要顺着这道往前走你就能去京城，去了京城办好了事情之后就去找顺风镖局，给镖头看官印，文牒，镖头自然会护送你回金陵城，老子要回去救马东，牛二他们。”
    此时此刻，吴非骑着健马奔走到了大道上，一把放下朱昱便转身离开，骑着健马很快就绝尘而去。
    朱昱定了定神，对着远去的吴非喊道；“小心一点。”
    说完，他又对着远处弯腰一拜。
    他知道，这一次李兄身边的这几个护卫是凶多吉少了。
    “李修远你且放心，这次的事情我一定会办好的，绝不会辜负你的希望。”朱昱咬着牙信誓旦旦的说道，摸了摸胸口的包裹，毅然决然的往京城而去。
    “李修远？哦，你这书生是李修远的朋友？”
    官道之上，一个迎面走来的中年汉子听到这朱昱的话，不禁开口问了一句。
    “你，你是谁？”朱昱一下子警惕了起来，捂着心口，警惕不易的盯着他。
    “你不必紧张，我叫夏侯武，是一位剑客游侠，前几日砍了一个贼匪的脑袋刚刚拿到京城六扇门领赏银，李修远是我的恩人，他曾救过我的性命，今日路过的时候提到你提起他的名字，故此多问了一句。”这个叫夏侯武的中年男子说道。
    “你就是那个和燕赤霞比剑七八年，都赢不了燕赤霞一次的百败剑客？小生听宁采臣提起过。”朱昱说道。
    “......”夏侯武嘴角一抽。
    百败剑客？
    自己什么时候多出了这么一个名号，而自己输给燕赤霞的事情连读书人都知道了。
    再不赢燕赤霞，自己怕是要遗臭万年了。
    “你这书生无趣，不如李修远爽快，不与你多说了，我要去寻燕赤霞比剑，不陪你了，告辞。”夏侯武瞪了一眼，便欲离开。
    朱昱却急忙抓住了他的手臂道：“夏侯武你不能走，现在有一件人命关天的大事，正是你报答李兄的时候了。”
    “是什么急事？”夏侯武问道。
