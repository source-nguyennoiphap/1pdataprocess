    兰若寺的禅房院子里。
    一位看上去有些柔弱，老实的书生这个时候正捧着一卷书卷在院子里走在走去，时不时的念起了几句诗句。
    “床前明月光，疑是地上霜。”
    “君不见黄河之水天上来......”
    诗句前一句不搭后一句，声音带着几分颤抖，脑袋东张西望，不断的注意着周围的一举一动，看他连紧张不已的样子，便连手中的书籍拿反了也不知道。
    院子的四角都插着灯笼，里面烛光映出，到是显得周围并不昏暗。
    只是这寺庙之中寂静无声，一个人影都没有，周围安静的有些诡异。
    “不，不会真的有吃人的恶鬼吧。”宁采臣心中害怕的要死。
    他是一个胆子不大的人，对于这鬼怪更是畏惧三分，连之前走夜路都需要打上三个灯笼才敢走。
    “李公子，你在哪，你们别走远了。”宁采臣在院子里走了不到半个时辰，便有些抗不住了，开始呼喊起来。
    可是周围安静一片，没有人应他的话。
    在兰若寺的一座宝刹之中，李修远，燕赤霞，夏侯武三个人居高临下，看着禅院内发生的一切，那宁采臣在院子里瑟瑟发抖的吟诗作对的样子也看的分外清楚，似乎只要有一丁点的风吹草动都能立刻冲过去支援他。
    “这个诱饵能行么？那妖怪真的能上当？”燕赤霞睁大了眼睛，撑着醉意道。
    李修远道：“肯定能行，树妖是忌惮你所以才一直没有露面，但是这根本原因是因为和你争斗得不到好处，如果有足够的好处说服树妖的话，树妖还是愿意出手的，它有着千年的道行，虽然忌惮你，但却并不害怕，眼下宁采臣一个凡人在这里当诱饵，就如同鱼儿入了网，树妖岂能忍得住。”
    “到时候可别把他的性命给害了。”燕赤霞说道。
    “不会，我离开的时候给了他一根针，遇到树妖多少有点自保的力量。”李修远道：“再说了，我们就在这里看着，一等树妖出现我们就一起出手，不会让它有时间害人的。”
    “那个树妖，我一定要把它劈了当拆烧。”夏侯武握着宝剑，心中愤愤不平，随时准备出手。
    李修远见此也不由笑了笑，三个人联手怎么可能拿不住树妖。
    只要自己靠近了树妖，立刻就封锁它的法力，让它没办法逃遁，到时候只需一把火，就能将树妖杀死。
    连斩仙大刀都不需要祭出来。
    手段是有，关键这树妖得出来才行。
    “李公子，你在哪？你回答我啊。”
    宁采臣见到院子外黑漆漆的一片，没人回应自己，心中更慌了。
    “早知道就不应该答应李公子，来当诱饵的。”他心中悲凉的说道。
    记得当时李修远拍着他的肩膀说：“斩除妖魔，为的是维护世界的和平，拯救万民于水火，这是义不容辞的事情，纵百死而无悔，国荣啊，如今我们已经做好了万全的准备，就少一个人充当诱饵将那妖魔鬼怪吸引出来了，相信为了维护世界的和平，为了拯救万民于水火，这个小忙你一定会答应我的。”
    被这一忽悠，宁采臣脑袋一热就答应了下来。
    结果就成了现在这个样子，一个人孤零零的在院子里吟诗作对，担惊受怕，时时刻刻要防范着可能出现的恶鬼，恶妖。
    “啪嗒~！”
    夜渐渐深了，可就在这个时候，忽的附近禅房的屋顶上似乎有什么东西滚落了下来，落在了院子里，发出了一声清脆的声响。
    “啊~!”
    宁采臣吓的尖叫一声，脸色瞬间就苍白了起来，下意识的寻声看去。
    “呸，这书生胆子真小，到时候肯定要坏了我们的事情，妖怪见到他这样子肯定知道有古怪。”夏侯武说道。
    “不，鬼怪已经来了，你看掉落在地上的那东西。”燕赤霞身子一震，醉意瞬间荡然无存，然后眯着眼睛看向了禅房的院子里。
    夏侯武认认真真的看去，却道：“好打一锭金元宝。”
    从禅房的屋顶上滚落下来的竟是一锭金光闪闪的金元宝。
    “哪是什么元宝，是一块人骨，这是障眼法，鬼怪是想用金钱诱惑宁采臣，只要他动了贪念，就会被鬼怪乘虚而入。”李修远道。
    “嘘，小声点，那玩意来了。”燕赤霞粗中有细，此刻却是耐着性子静静等候。
    当院子里的宁采臣见到落在院子里的那锭金子的时候，不禁微微松了口气：“李公子真是有钱，竟舍得拿一锭金子丢过来提醒我，嗯，看来李公子就在我不远处躲藏着，如此小生也就放心了”
    他见到那金子，只以为是李修远丢的。
    毕竟这李家土豪，丢金子又不是头一回了，上次在郭北城也丢过。
    于是乎，宁采臣反而镇定了不少，继续吟唱诗句起来。
    “呼呼~！”
    但是随后夜里一阵奇怪的风吹来，这股风来的十分的突兀，又比其他的风要阴冷不少，像是入冬的寒风，能吹透衣物，侵入皮肉之中去。
    “好冷。”
    宁采臣打了个哆嗦
    “哗啦啦.....”阴风吹来，挂在院子里四个角的四个灯笼立刻剧烈的摆动了起来，紧接着一个个灯笼啪嗒一声接二连三的摔在了地上，
    瞬间，所有的灯笼全部熄灭了。
    院子里当即就昏暗了起来。
    “鬼啊。”
    宁采臣吓的大叫一声，逃似的跑进了身后的禅房里，然后迅速的把门就给关上了，转眼之间就不知道躲到哪个角落里瑟瑟发抖去了。
    “呼呼~！”
    阴风吹起，一位身穿艳丽衣衫，精心打扮，画着淡妆的女子竟从兰若寺后山的方向缓缓飘来，最后竟落在了刚才宁采臣所在的院子里。
    “女鬼出现了，之前就是这女鬼差点害了我的性命，居然还敢来，我去杀了她。”宝刹之内的夏侯武见此，当即忍不住就想提剑杀出去。
    李修远制止了他：“诶，夏侯兄，这只是一只打头阵的小女鬼而已，在她背后的那个千年树妖才是我们真的敌人，放长线，钓大鱼，等那老妖出现我们才一起杀出去，得有点耐心。”
    “好，好吧。”夏侯武性格急躁，耐心不足，被李修远这么一说方才按捺了下来。
    “那女鬼不是青梅.....那么很有可能是聂小倩，难怪夏侯武会中招，这姿色的确有倾国之貌，只要是还有七情六欲的人，难保不会对其动心。”
    李修远远远看去，虽看的不是特别清晰，但也能看出一个大致的模样。
    但他却并没有去阻止聂小倩和宁采臣的第一次相见，毕竟眼下不能打草惊蛇。
    “唐代诗集。”聂小倩来到院子，见到地上的一本书，捡起来一看，却是唐代各大才子所做的诗文。
    “相见时难别亦难，东风无力百花残......”
    聂小倩翻看一看，忍不住轻轻低喃了一句，她身前是才女，纵然死后这么多年，以前的东西亦是没有忘记。
    但很快，她从回忆之中醒来，幽幽一叹，然后向着禅房走去。
    “公子，公子在屋里么？快开门啊，小女子路过这里，被饿狼追赶，还请公子开门相救。”聂小倩伤感收敛一空，立刻改变了神态，露出了焦急和恐慌的样子。
    夜晚，任凭是谁有一点良善的人，听到一位弱女子这样也肯定会心动的。
    “姑，姑娘，你是人，还是鬼啊。”屋内，宁采臣忍不住哆哆嗦嗦的回了一句。
    他怕如果真的是人的话，自己不开门相救，岂不是害了一位姑娘？
    这对他而言是不能接受的。
