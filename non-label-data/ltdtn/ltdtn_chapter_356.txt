    看着眼前这个身穿朱红色官服的鬼神，如果不是亲眼见到他的相貌，几位鬼王是很难相信这就是陆判。
    这可是传说之中阴间的判官，带勾生死簿，主管人间生死的鬼。
    无论是身份，地位还是道行都是一等一。
    而且谋略也足，和人间圣人争斗许久从未被诛杀过。
    “真的是陆判，没想到他也有今日这般的下场，长舌，你是怎么擒下他的？”红目鬼王问道。
    长舌鬼王说道：“非我一人之功，乃是这陆判蛰伏在那位钱总兵的军营之中，试图谋取吴象身体内的五头神象，结果神象出来的时候把他给撞飞了，你们看，陆判身上还冒着白光，那是神象身上菩萨留下的
    念力，他这等鬼神被菩萨的念力镇压着，连翻身的力气都没有。”
    几位鬼王看去，果真如此。
    陆判被区区几根鬼气加持的草环绑住了手竟倒在地上挣脱不开。
    这放在以前是想都不敢想的。
    “的确是菩萨的念力，哈哈，这个陆判合该有此一日。”几位鬼王大笑了起来。
    而在十王殿之中的李修远却是听到声音不由从入定之中醒来，然后大步走来：“哦，有什么事情值得你们三位鬼王高兴的？说出来让我也知晓知晓。”
    “见过公子。”
    三位鬼王齐齐势力。
    “回公子，事情是这样的，长舌鬼王在钱总兵的军营之中抓到了陆判，公子你看。”长须鬼王指着躺在地上的陆判道。
    李修远看了一眼，目光当即一凝：“陆判？”
    陆判此刻也看到了李修远，此刻不由咬牙切齿道：“李修远，你还没死啊，你这个人间圣人可真够命大的，本判官的三番五次的算计你皆能逢凶化吉。”
    “有些事情是你这种福德败坏的鬼神很难理解的。”
    李修远平静的说道：“上次我神魂出窍，手持斩仙大刀诛杀你，却被你遁入天宫，得了东岳神君的庇护逃走了，如今东岳神君已经被我从天宫之中斩落了下来，这次想来你的死期到了吧，不会有人再来救你
    了。”
    “不过是捡了个便宜罢了，算什么本事？”陆判怒气冲冲的说道：“若非本判官被那白象所伤怎么会被一个鬼王抓道。”
    李修远却是摇头笑道：“捡便宜？不，这个世界没有巧合，只有天意，你之前没有死只是因为东岳神君还没有倒，他的气运庇护了你，可是现在东岳神君坠下了凡尘，庇护你的气运没了，所以你立刻就落到
    了我的手中。”
    “看似巧合，里面涉及到了很多复杂的东西，你不修功德，不攒阴德，今日不落到我手中，明日，后日也会落到我的手中。”
    陆判冷哼道：“李修远说那么多废话做什么，要动手就动手，本判官岂是畏惧生死之鬼？”
    不怕死？
    李修远转而问道：“这个陆判也修了金身法相？能借助庙宇活下去？”
    “回公子，陆判庙宇是有，但是否修了金身法相小鬼却还是不清楚，不过有一法可以试之，还请公子稍等。”长须鬼王施了一礼，然后化作一股阴风呼啸而去。
    约莫片刻之后他带来了一尊神像。
    是陆判的神像。
    “陆判伏诛，若是这神像崩塌，他则死期到了，若是神像未崩塌，说明他还有香火分身。”长须鬼王道。
    李修远沉吟一下；“这倒是一个简单明了的法子，神明死去，神像都会跟着崩塌碎裂，此法的确能测出今日能否将这陆判彻底伏诛。”
    陆判此刻却是哈哈笑道：“李修远，你没了斩仙刀怎么杀鬼神啊，靠你的鬼神不近的本事么？那最多只是打散我的形体，并不能将我彻底诛灭，还是说你堂堂的人间圣人，要让天下鬼神应神仙杀劫的人物，
    却连对付鬼神的手段都没有？难不成还要靠属下动手不成。”
    “大胆陆判，你敢诋毁圣人。”
    “公子是统御鬼神的圣人，你见过人间的皇帝亲自行刑的么？”
    “公子，这陆判死到临头了且容小鬼将其诛灭。”
    三位鬼王顿时勃然大怒，恨不得将这陆判生吞活剥了。
    李修远却脸色平静，目中似有一道金光闪过，他大步走了过去，一脚踢到了陆判的身上：“你这样的恶鬼，何须让我祭刀，便是拳打脚踢也能将其诛灭。”
    “哈哈，圣人的拳脚本判官还未领教，希望你的力道够强......”陆判哈哈大笑，似乎不以为然，然而他的话到嘴边的时候却是陡然止住了。
    眸子一凝。
    他看见这一脚踢来隐约有金光闪动，那金光之中是一座巍峨的山岳，不，应该不是一座巍峨山岳应该是半座。
    “轰~！”
    下一刻，陆判的身躯骤然炸裂发出了一声凄惨的叫声，鬼神之躯直接飞了出去一下子就从地上飞到了高空之中，仿佛一道离弦的箭矢一样，接着在目光所及的最后地方，他的身躯直接炸开，只留下了斑驳的
    香火和阴气。
    嗯？
    三位鬼王见此一幕，眼皮直跳，心中胆寒。
    在李修远动手的那一刻，他们感觉到了一股巍峨磅礴的力量一闪而逝，这力量和那斩仙大刀不相伯仲，但给人的感觉却是截然不同。
    斩仙刀是锋利凌厉，仿佛能斩世间一切之物。
    但这股力量却是浩瀚雄伟，巍峨如山岳，厚重似大地，能镇压世间一切。
    “飞的还真够远的。”
    李修远也是心中一突，自己一脚踢过去什么时候力量变的这么可怕了。
    “咔嚓，咔嚓~！”
    随着陆判的鬼神之躯崩裂，消散，之前那长须鬼王取来的一座陆判的神像却是出现了阵阵龟裂，随后砰地一声直接崩塌了，成为了满地的泥块。
    “陆判的泥像崩塌了，他彻底的消亡了，溃散的躯体以后只能变成虫蚁，野草了，想要再修成人身至少要过三千年才行，先为野草，后为虫蚁，再做牲畜，最后才有可能他成为人，轮回怎么久，陆判已经不
    是陆判了，轮回九世之后就能磨灭第一世的一切痕迹，更别说轮回至少百世了。”长须鬼王开口道。
    “是么？不过我很好奇我为什么有这样的力量了？”李修远道皱眉道。
    他虽能击伤鬼神，但却不能如这般，直接拳脚诛杀一位有这样道行的鬼。
    “在仙宫的时候小鬼看见公子得了东岳神君的神权，这应该是东岳神君的神权力量吧。”红目鬼王道
    “是那半座金山么？”
    李修远记得很清楚，他身体之中的确是融入了从东岳神君上斩下来的半座金山。
    那是半座泰山的样子。
    “是的，那就是东岳神君的神权，拥有替天行道的力量，正是因为如此，东岳神君才能领万仙，统群鬼，主生死。”红目鬼王道。
    “说的这么玄乎，能具体一些么？得了这神权之后我有什么本事。”李修远道。
    红目鬼王道：“这个......不清楚。”
    “.......”李修远神色古怪的看着他。
    不知道你说的那么带劲做什么。
    长舌鬼王道：“公子，小鬼以前在阴间听阎君说起过，神权某种程度上代表着天意，是无所不能的。”
    “无所不能么？”李修远道：“如果我想看看身在郭北县的小蝶和杜春花呢？”
    “应当可以吧......”长舌鬼王有些不确信道。
    可是他的话还未说完，突然李修远双目之中金光一闪，随后眼前的场景走马观花一般迅速的闪过，最后突然定格在了郭北县他所在的卧房里。
    此刻，卧榻之上，却见一个清秀可人的丫鬟和一位白皙年轻的成熟女子相拥在一起，正在酣睡着。
    “这是.......家中的情况。”李修远脸上露出了惊容，他居然真的看见了家中这两个丫鬟，婢女的样子。
    “公子，这是神目术，和菩萨的天眼通，道家的玄光术是一样的，能看见千里之外的情况，公子得了东岳神君的神权，这样的法术不难施展出来。”长舌鬼王的声音在耳旁响起。
    由此可见李修远的人还在金陵城的十王殿内，但是眼中的景象却是倒影着家中的一切。
    法术的玄妙，真是难以置信。
    而事实的确如此，此刻李修远身子依然站在原地，他的眼中冒着金光，至于看到了什么别人是不知晓的。
    “原来如此，不过我并未施展法术，只是心想事成而已，这就是神权的力量么？”李修远心中暗暗心惊，这下他明白了为什么当日天宫之中会有鬼神争夺神权了。
    “去兰若寺。”
    他眼睛一避，一睁，下一刻却到了兰若寺的上空，俯瞰寺庙的一切。
    “去望川山。”李修远再次心念一动，眼中金光闪烁，却是看到了山中那个吴非生活的山中村落。
    “去郭北城。”
    他感觉神奇的很，忍不住多试了几次然后又来到了郭北城的上空。
    看着城内点点灯光，他有种俯瞰芸芸众生的感觉。
    这或许就是做神的感觉了。
    忽的，他想到了什么目光一移，却是挪到了郭北城外的城隍庙上。
    此时此刻，城隍庙的屋顶上，一位道人正在闭目盘坐，迎着月光汲取精华。
    那是瞎道人。
    蓦地。
    瞎道人似乎察觉到了什么，睁开了眼睛，抬起头看着天空：“徒儿，你瞅啥，三更半夜的不睡觉偷窥师傅做甚？”
