    让郝昭这个武艺非凡之人跪在地上磕头的不是赵瑞个人的威严。
    让他像一条狗一样跪在地上的是这世道，是这皇权。
    郝昭也不是向赵瑞妥协，求饶，是没办法反抗这世道，反抗不了皇权。
    莫说是他，便是李修远也深深的明白，敢诛杀一位王侯，除了造反之外没有第二条路可以走，但对郝昭这个有家室，有父母，有亲朋的人而言，让他造反比杀了他还难。
    即便是他的武艺强大到可以将这整个驿站的人全部杀光，全部灭口。
    那又如何？
    当今官家的弟弟，大宋国的越王被刺杀于官道附近的驿站之内，朝廷能不震动？能不彻查？
    六扇门的捕头，朝廷供奉的一些法师，修道之人，想要找出杀人凶手简直就是轻而易举。
    因为明白，所以郝昭才跪在地上磕头。
    而且最为重要的是，这个赵瑞性格疯狂，根本就不怕死亡威胁，郝昭的刀架在他的脖子上他非但不害怕，反而抓着他的刀自己把脖子送上去。
    如果不是郝昭当时收力了，赵瑞就已经自杀身亡。
    李修远正是因为看到这种种一切，所以他才一直在皱眉思考，希望想出一个万全之策。
    既能保全这郝昭一家，又能不让这个赵瑞记恨他们。
    替死法就是李修远觉得最为稳妥的法术。
    因为没有人会对死人记仇。
    “你磕头磕的不错，本王心情好了许多。”赵瑞站了起来缓缓的走到了郝昭的面前。
    “王爷，当心啊。”柳先生大惊，忙指挥护卫保护。
    “滚开。”赵瑞一脚踹开了柳先生还有几个护卫：“本王不要你们这群废物保护。”
    旁边的护卫面带畏惧，又怯怯的退了下来。
    赵瑞走到郝昭的面前拍着他的脸庞道：“本王知道，你很想杀了我，但是本王还知道，你不敢，因为以你的武艺要杀本王有着无数次的机会，可是你依然没有动手，因为其中原因很简单，本王姓赵，赵官家
    的赵，你这个贱民脑子不错，知道杀了本王之后的代价是无法承受的。”
    “当然，如果你要改变注意的话现在也不晚，本王的脖子就在这里，刀还在你的手中，护卫本王都给支开了，杀了本王你就能带着你的妻子远走高飞。”
    赵瑞面带笑容有些冰冷：“只是天下是姓赵的，你一家老小能走到哪去？”
    “你....说过要放过我们的。”郝昭从牙缝之中蹦出了几个字：“你出尔反尔的话，我立刻就杀了你。”
    “哈哈，不错，本王没说过要放过你们，只是说了可以考虑，看在你刚才磕头的份上，本王可以给你一个机会。嗯，本王想一下，有了，你就陪本王玩一个游戏吧，如果你赢了，本王就放过你妻子，让你们
    离开，也不会追究你们的罪名。”
    “如果你输了......”赵瑞说道：“那就对不住了，本王要你妻子的命。”
    “你这狗东西。”
    郝昭怒目暴起，刀又搁在了赵瑞的脖子上。
    “哈哈，杀吧，想杀的话就杀吧，但本王可以断定你这贱民不敢杀本王。”
    赵瑞哈哈大笑，根本不怕那架在脖子上的刀锋。
    “你现在只有两个选择，要么杀了本王被诛连九族，要么就陪本王玩这个游戏，只有赢了你才可以安然无恙，当然，如果你带着你妻子跑了，不陪本王玩，本王照样会下令通缉你们。”
    郝昭咬着牙，手中的钢刀只需一用力就能轻易的割下这个王爷的人头。
    可是他的刀依然是砍不下去。
    一旁的妻子郝氏则是哭着道：“官家，答应他吧，贱内的生死已经不重要了，但是官家你却不能再有事啊，家里如果没有你那就一切都完了，以后公婆会饿死，孩子会饿死，什么都没了。”
    “看看，你的妻子多么贤惠，这个时候了还在为你担心，你的刀难道就只是杀人的么？也许以你的本事能赢过本王也说不定呢？”赵瑞笑道。
    “可恶。”
    郝昭手中的钢刀一甩，只听见一声炸响，腰刀没入了旁边的青砖之中。
    “你这狗官，真要把我逼上绝路才肯罢休么？你想玩什么，我陪你玩便是，但若是你再出尔反尔的话，我发誓，今日一定要割了你的脑袋。”
    他对打天发誓，咬牙切齿的怒吼道。
    “这就对了，不过你放心，本王可是一向很讲信用的，本王是当今王爷，怎么会和你们这些贱民一样说话不算数？”
    赵瑞见其答应，一下子欣喜起来，仿佛就是在等这一刻。
    “那么，游戏开始了。”
    赵瑞兴奋的说道，然后开始踱步思考起来，不断嘀咕道：今天玩什么呢？玩什么呢？
    “你这狗东西快些。”郝昭急不可耐的催促道。
    “别急，别急，难得今日遇上你这么一个有趣的人，本王自然要好好玩玩才行。”赵瑞对他的辱骂丝毫不放在心上。
    忽的，他猛地抬起头道：“有了。”
    随后他指着一个护卫道：“你，对，就是你，给本王走出驿站，别走远，出去之后从驿站外绕一圈再进来。”
    “是，王爷。”
    那护卫楞了一下，随后应了下来，有些心惊肉跳的转身离去。
    他也不知道这个王爷今日又要玩什么游戏。
    待那护卫离开之后，赵瑞又兴奋的说道；“郝昭，本王的游戏开始了，这是一个猜谜游戏，你猜猜看本王的那个护卫待会儿绕一圈走进来之后是左脚先踏过门槛，还是右脚先踏过门槛呢？如果你猜中了，本
    王就放了你和你妻子，如果你猜错了，你妻子今日就死定了。”
    疯，疯子，这个人是一个疯子。
    郝昭感觉手脚冰凉，看着赵瑞脸上那兴奋的神色，脑海之中就只有这么一个想法。
    宛如孩童一般幼稚的游戏，竟被他拿人命做赌注。
    “快猜，听见门外的脚步声没有，本王的护卫已经绕了半圈了，马上就要进来了，如果你不猜的话本王可就先猜了。”赵瑞催促道。
    郝昭捏着拳头，额头上冷汗直冒，心中挣扎万分。
    他嘴唇微动，想要说出口，却怎么也说不出来话。
    一旦猜错了，自己的妻子可就死定了。
    这样儿戏般的决定自己妻子的性命，他怎么忍心说出口。
    “踏踏，踏踏。”
    适才离开的那个护卫已经转了快一圈了，脚步声已经从身后清晰的传了过来，只需要几个呼吸的时间那个护卫就要走进驿站里了。
    赵瑞拍着大腿道：“你这个人还真是婆婆妈妈的，一个这么简单的问题你居然要想这么久？你既然不猜那本王就先猜了，不过如果本王猜中了的话，这个游戏你可就输了，嗯，本王猜那个护卫是左脚先迈进
    来的。”
    话才一说完，之前离开的那个护卫就已经走到了门口，此刻抱了一拳：“王爷。”
    郝昭感觉自己此刻就要被逼疯了，他心头直颤，不由的转过头去死死的盯着那个护卫的双脚。
    他到底没有勇气拿自己的妻子的性命做赌注。
    可是这个王爷赵瑞却根本不给他逃避的机会。
    “进来。”赵瑞唤那护卫进来。
    “是，王爷。”护卫也有些摸不着头脑，大步便走了进来。
    这个护卫发现，此刻驿站的所有人都盯着自己看，让他感觉有些紧张不已，不知道自己走出去的这段时间这里又发生了什么事情。
    可就在他要迈过门槛的时候，突然神情恍惚了一下，脚步一顿，但随后还是伸脚迈过了门槛。
    而他迈过门槛的脚是......右脚。
