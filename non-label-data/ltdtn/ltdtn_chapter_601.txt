    夏侯武此刻气喘吁吁的跑来，惊魂未定，浑身狼狈不已，连上衣都没了。
    “夏侯兄，这三更半夜的不穿衣服就到处跑可是有伤风化的，是一件很丢人的事情，如果在城里，你可是要要被抓起来关进大牢的。”燕赤霞醉醺醺的笑道。
    “这里有妖怪，还有艳鬼，我刚才全部都遇到了，差点死在了妖怪的手中。”夏侯武立刻走进来说道。
    果然~！
    李修远心中一凛，他之前就猜到了夏侯武会遇到妖怪，看来事情真的如他想象中的一样。
    “刚才多亏了你的马救了我，不然我刚才已经死了，大恩不言谢，以后必有厚报。”夏侯武平复了一下心情，对着李修远感激的拱手道。
    李修远说道：“这只是举手之劳而已，相信任何一个良善的人都会出手相助的。”
    夏侯武见到他心胸如此大方，顿时满脸愧色道：“你不但武艺胜过我，情义也让我钦佩，悔不该之前没有听你的建议留在兰若寺。”
    “事情既然已经过去了就不用再提了，我来这里就是为了诛灭这里的妖怪，刚才夏侯兄遇到了妖怪，不知道是什么样的情况，能和我说说么？”李修远道。
    “说起来惭愧，是我定性不够，意志不够坚定被女鬼所迷惑了，险些丢了性命......”夏侯武对于之前的事情虽然难以启齿，但还是一五一十的说了出来。
    旁边的燕赤霞打了一个酒嗝：“如此说来，这里不但是女鬼，还有可能有一只树妖了？”
    “他们是一伙的，我想这里应该还有别的鬼魅。”夏侯武说道。
    “那正好，既然鬼怪一窝，那就把他们一网打尽，一个不留。”燕赤霞站了起来，取了宝剑道：“酒喝足了，走，书生我们去除妖。”
    说完便晃晃悠悠的往外走去。
    李修远说道：“你这般走出去，鬼怪吓都要吓跑了，从之前到现在我们喝酒已经喝了好几个时辰了，可见到有鬼怪出现？想来鬼怪是畏惧你已经躲起来了。”
    “那我就找他出来。”燕赤霞说道，然后便消失在夜里。
    李修远摇了摇头，也没有去制止，反正以燕赤霞的道行树妖想要一时半活儿的拿下他是不太可能的。
    “本想守株待兔，等那树妖找上门的，现在看来得改变方法了，让燕赤霞把那树妖赶出来。”
    旁边的夏侯武道：“那鬼怪差点害了我的性命，和我结下了深仇大恩，我也去寻那妖怪，定要一剑把它给杀了。”
    受到了燕赤霞的刺激，他不甘示弱，拿了剑便欲离去。
    李修远叫住了他：“夏侯兄且留步，燕赤霞他去寻妖怪因为他有对付妖怪的手段，所以妖怪畏惧他，你虽有一身过人的武艺，但是却没有对付妖怪的手段，这样冒然前去的话肯定会遇到危险的。”
    “那妖怪也没什么厉害的，我刚才杀出来的时候砍掉了它不少的根须，它若真厉害我也就逃不回来了。”夏侯武说道。
    李修远摇头道：“话不能这样说，羚羊受到了威胁转身搏命连猛虎都要退避三舍，但血气之勇一旦散去，羚羊照样会被猛虎吃掉，我猜树妖已经盯上了你，把你视作了是猎物，不可能轻易放过你的。”
    “既然如此，那就和那树妖拼命，我也不惧死。”夏侯武说道，还是坚决要去寻那树妖报仇。
    显然，他心高气傲，很不服气。
    “既然夏侯兄执意要去对付树妖的话，我有一驱鬼除妖的法子可以交给你，希望能对你有帮助。”李修远道。
    “什么法子？”
    “你把你的佩剑给我。”李修远道。
    夏侯武当即把佩剑递给了李修远。
    李修远取出锋利的宝剑，轻轻划破手指，用自己的血在宝剑上书写了一番，写了几个字：天大地大，道理最大。
    “好了，现在这把剑已经成为了一把能够诛杀妖魔的道理之剑，你再遇到妖魔鬼怪拿这把剑和它去讲道理，它必定十分畏惧。”
    “这能行......”夏侯武一脸古怪道。
    “当然行，如果你再能稳住心神不被鬼魅诱惑的话，那么便很少有妖魔鬼怪能挡住你的。”李修远道。
    夏侯武点了点头，正准备离开。
    这个时候寺庙外却又响起了一个男子的声音：“救命啊，救命啊，这里有人么？谁来救救我啊。”
    “定是妖怪出现了，我去将它给诛杀，以报刚才之仇。”
    夏侯武听到这个声音眼睛一亮，立刻冲了出去。
    “这声音似乎有些熟悉。”
    李修远却是心中有几分好奇，也跟过去看看。
    此刻，寺庙外，一位书生提着三四个灯笼，惊慌失措的往兰若寺跑来。在他的身后有三五头野狼紧随不舍，嚎叫不断，只等这个书生力气用尽之后便一拥而将他给分食了。
    “啊~！”
    书生来到兰若寺前，被山门的台阶一绊脚，惊呼一声摔倒在了地上，三四个灯笼顿时全部熄灭了，周围顿时变的昏暗了起来，只有天上的月光还勉强让他看得见那黑夜之中一双双发光的绿油油眼睛。
    “簌簌。”
    身后的饿狼见到了机会顿时一拥而上，准备捕食这个书生。
    “什么人，三更半夜的还在这荒郊野外的游逛。”
    就在饿狼准备扑上来的时候，一个醉醺醺的大嗓门响起，却见寺庙的大门砰地一声被踢开了，一位身材魁梧的络腮胡大汉手提宝剑走了出来。
    “野狼滚开。”
    燕赤霞见到饿狼要吃人，立刻拔出宝剑掷出，随着宝剑飞出直接将前面的一匹饿狼给射穿了，然后大步冲了过去，欲斩杀其他饿狼，以他的武艺对付这些饿狼就好像杀鸡屠狗一般轻松。
    然而不等靠近，剩下的饿狼就见势不妙全部溜走了。
    “呸，这么胆小的狼，还以为是狗呢。”燕赤霞见到野狼溜走，有些很不过瘾的说道。
    “你一个手无缚鸡之力的书生这么晚了跑这里做什么？赶紧离开，这里不是你待的地方。”
    打量了一下这书生，见到他是一个手无缚鸡的寻常人，燕赤霞立刻轰他离开。
    虽态度恶劣，但这却是为他好。
    “晚生宁采臣，多谢这位壮士出手相救，晚生只想在寺庙里借宿一晚上，绝对不会打搅贵寺，还请通融。”宁采臣忙施了一礼。
    “我不是这寺庙的主人，这寺庙的和尚全死光了，现在是一座空庙，废庙，我也是在这里借宿的，不过这里有不干净的东西，你不想死的话就赶紧离去。”燕赤霞取了宝剑，转身离去。
    他不是一个婆婆妈妈的人，不喜欢劝人。
    “燕赤霞和这妖怪费什么话，三更半夜怎么可能会有人在外面走路，定是鬼魅变化而成，待我一剑斩了他，让他原形毕露。”这个时候夏侯武闻声赶来，见到宁采臣这个书生，只把他当做了鬼魅，立刻拔剑便刺向了宁采臣。
    “铿~！”
    一声碰撞声响起，燕赤霞挡下了这一剑。
    “夏侯兄，你人鬼不分的么？这是一个普通人，你杀了他就错杀了好人。”燕赤霞说道。
    “什么，他是人？”夏侯武诧异道。
    “当然是人，他身上没有妖气，鬼气，不是人是什么。”燕赤霞说道。
    “他的确是人，不是鬼魅，也不是妖怪，而是我的一位好友，他名叫宁采臣，是一位童生，嗯，或许已经是一位秀才了。”
    李修远走了过来，见到宁采臣的时候也是心头一跳。
    这也太巧合了吧，所有的人物居然都聚齐了，就差一场好戏上演了。
    “李，李公子。”
    宁采臣见到李修远又惊又喜：“李公子，你怎么在这里，你不是回郭北县去了么。”
    “我特意来这里诛妖，当然在这里，到是你，不是在郭北城待着么，怎么跑到这里来做什么。”李修远道。
    宁采臣说道；“小生之前在集宝斋抄书，今日早上掌柜的因为手底下没使唤的下人了，所以托我来郭北城收账，正巧也有一件事情要告知李公子，所以小生便应下了此事。”
    “是什么事情？”李修远问道。
    “是放榜的事情，因为瘟疫的缘故这次朝廷选择在金陵城放榜，榜文已经下来了，李公子应当还不知道这件事情吧，城里不少的书生都去金陵城看榜了。”宁采臣说道。
    “原来如此，有劳你特意跑来一趟告知了。”李修远感激道。
    宁采臣有些受宠若惊道：“只是顺道带句话而已，李公子客气了。”
    “既然他不是妖怪，那真正的妖怪躲哪里去了？”夏侯武有些暴躁道。
    燕赤霞说道：“谁知道，这里遍地都是妖味，肯定是妖怪的老巢，那树妖迟早会出现的，有点耐心等等就是了。”
    “这样等下去的话不是办法，我们需要一个诱饵引那树妖出现，这个诱饵一定是普通人，这样才不会引起鬼怪的警惕。”李修远摸了摸下巴道。
    诱饵？
    好主意~！
    瞬间。
    夏侯武，燕赤霞，还有李修远皆齐刷刷的看向了宁采臣。
    “？？”宁采臣一脸的茫然。
