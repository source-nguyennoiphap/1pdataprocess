    术是小术，符是真言符。
    能让人口吐真言，将内心所有的秘密都说出来。
    凌风子就是要让眼前这个书生自己揭露自己的谎言，让其颜面尽失，知道欺骗道家高人的后果。
    虽说真言符并非什么恶术，但也得看怎么用了，用在凡人身上，岂会挖掘不出一些难以启齿的亏心之事？
    毕竟人无完人。
    然而道符迎着李修远飞去的时候，本是能落到人的身上立刻贴粘上去，若不施法是取不下的，然而让凌风子傻眼的是，自己的道符飞到一半的时候却轻飘飘的失去了力量，晃晃悠悠的掉落了下来，最后落在
    了地上一动不动。
    法术失灵了？
    凌风子心中一慌，不疑其他，只怀疑自己学艺不精这次施法没有成功，当即脸色一红，有羞愧之一，随后低喝一声，伸出手指对着那地上的黄符一指：“起~!”
    可是纸符却依然一动不动，没有回应他。
    “道长这是做什么？好端端的为何那黄纸丢我，莫不是道长新学的法术？”李修远问道。
    凌风子顿时怒瞪一眼道：“你这恶贼，用了什么手段竟污了我的符纸，还我的法术失灵了？”
    “道长，修道之人可切记生怒啊，怒是心中火，能烧阴魂，损道行。”李修远认真道：“况且我也并未施展什么手段，也许是道长的符没有画端正，失了水准也说不定，不过在下觉得是道长信念不够真诚，
    所以失了灵验。”
    “古人曾言，心诚则灵，法术也是如此，故而在下认为法术的使用是用在正途上才会有威力，因为念头正了，信念才会真诚，若是用在歪门邪道上，心思不纯，法术自然就会失灵了。”
    “莫不是刚才道长用符法是想做什么坏事？”
    凌风子闻言又急又怒，脸上臊红一片，不由想起了自己师傅的嘱咐，法术的确是不能用在害人，牟利上，否则会遭报应。
    “放屁，贫道是修道之人，只是见你这书生居心不良故而略施小术想让你说出真话罢了，岂会有害人之心。”他怎么会承认自己的这种心思。
    “术不是旁门左道之术，但人却不一定了，你叫这纸符他不答应，我叫它的话定有回应？道长可信。”李修远说道。
    凌风子说道：“区区一凡人也认识仙家法术？满嘴狂言，你若能叫起这符，贫道给你下跪。”
    “下跪就不用了，我这个人不太喜欢人跪。”李修远道，然后看了一眼地上的纸符，接着伸手一指。
    “且看好了，什么叫心诚则灵。”
    当即，不但这道人，便是旁边的成乐和周才二人也是好奇的看着地上的那纸符，心中惊疑起来，不知道李修远是装腔作势，还是真有本事？
    “起~！”然而李修远齐齐一喝，地上的不动的纸符竟一跃而起落到了他的手中。
    在凌风子手中不灵验的法术，竟在他手中灵验了。
    “怎么可能？”凌风子大惊。
    这可是他的符啊。
    “如何？若是不信的话道长可以试试这符的是否灵验？”李修远伸手一指，黄符飞出，欲贴到凌风子的身上。
    “不，不用了。”凌风子急忙道，神色有些慌张。
    “哦，是么？那便算了，既然道长相信就足够了，毕竟得饶人处且饶人嘛。”李修远道。
    随后纸符又轻飘飘的落了下来。
    凌风子微微松了口气，他这才收了几份轻视之心，看了看李修远，猜测这个人是不是学过法术？可是怎么看却都不像是学过法术的样子，真是奇怪了。
    可是见到李修远那一副似笑非笑的淡然样子，他却又气不打一处来，知觉这书生落了自己的面皮，害自己颜面尽失。
    可恶的家伙~！
    他心中暗暗生怒，狠狠的瞪了一眼。
    “道长，息怒，息怒，这位李兄是新来长寿镇的，不知道长的身份，还请道长莫要与他一般计较。”成乐见到凌风子的脸色不对，急忙站出来做劝解道，不想惹恼了这位高人。
    凌风子重重一哼：“你们这些人没有修道的诚心，明日不准你们上山，你们从哪来回哪去吧，仙家福地不会收你们这些人当弟子的。”
    “这，这个......道长，我等是真心求仙问道的，还请道长网开一面啊。”成乐当即慌张了起来。
    果然，这李兄结下了恩怨，惹恼了这个道长。
    “哼，贫道说不准上山就不准上山，难不成还要贫道说第二遍么？”凌风子重重的一甩道袍，冷着脸道。
    “道长，凌风子道长，还请给我们一次机会吧，李兄真的不是故意得罪道长的。”成乐恳求道。
    凌风子冷冷的瞥了一眼李修远：“你们和这样的人为伍还想求仙问道？本来你们是有仙缘的，可是碰到此人，现在仙缘没了。”
    “怎会如此，怎会如此.....”成乐一时间不知所措，喃喃自语，一副失魂落魄，绝望无比的样子。
    他并不责怪李修远，毕竟此事他看在眼中，是这道长惹出来的，可是眼下是求仙问道的关键时刻，又怎么好得罪这位道长，说他的不是呢？
    难道自己天生就注定休不了道，成不了仙么？
    李修远见此看不下去了，他道：“能不能求仙问道，能不能上山，道长怕是说了不算吧？”
    “那你这人就说错了，贫道负责世俗之事，也负责引人入山门求仙，若无贫道的指引，你们连山门都进不去。”凌风子淡淡的说道，神情有些倨傲。
    山中有法术遮蔽道路，凡人肉眼凡胎是找不到正确路的，只会在山上转了一圈之后又回到山脚下。
    “这就对了，你是世俗之人，管世俗之事，所以一身俗气，仗着微薄法术，卖弄显摆，故而你也并非正在的仙家高人，我等求仙问道求的不是你的仙，问的不是你的道，你如何能管？至于入山门，这就不劳
    道长费心了，山在眼前，路在脚下，我等会自行前去。”李修远道。
    凌风子怒极而笑：“好，好得很，贫道倒要看看你这三人明日如何上山求仙问道，掌柜的，按以往的规矩，装上三大坛酒，待会儿贫道还要送上山去，今日遇到三个俗人，还需早点回去，免得影响了修行。
    ”
    说完，不再理会这几人，从腰间解下一葫芦，递给掌柜。
    掌柜恭恭敬敬的应了声，便吩咐小厮去打酒。
    却见那酒水不断灌入葫芦之中，小小的葫芦却就是一个无底洞一样怎么样也灌不满，不多时，一缸酒就已经倒尽。
    “这是神仙法术啊。”一旁的周才见此，惊叹起来。
    “这葫芦是仙家宝物，岂是你们凡人可以知晓了，莫说三缸酒，便是五湖四海之水也能尽数装下。”凌风子闻言微微有些自得道。
    这葫芦不是他的，是他师傅的。
    因为平日好酒，故而每隔一段时间他都会来长寿镇打酒送去。
    “虽是一件宝物，但装五湖四海之水就有些夸张了，我看顶多就能装十缸水罢了。”李修远摇头道。
    “他是如何知晓了？”
    凌风子闻言，心中大惊。
    此人说的不假，师傅的这葫芦的确最多只能装十大缸水。
    不过这也算是极多的了，用来装酒那是万难装满的，毕竟酒贵，装满葫芦，便是银钱也得花费良多。
    修道之人钱财拮据，所以装满的情况是极难发生的，故而他吹嘘能装五湖四海之水凡人亦是深信不疑，闻言之人无不惊叹羡慕、
    却不料今日被这个古怪的书生一语看破。
    李修远之所以知道，当然是因为自己的师傅也有一个破葫芦，是以前出摊算命时候盛水喝的。
    小时候他也听那师傅吹嘘过，这葫芦能装一条江河之水，不过他不信，拿葫芦去池塘边试了试，结果发现是骗人的，连一个小池塘都不能装满。
    至于后来.......没后来了，那葫芦坏掉了。
    池塘中多有妇人洗衣，有污秽之气，葫芦染了污秽之气没几日就烂掉了。
    所以这种葫芦只能装没有污染过的井水，酒水，山泉之水，忌讳还是有不少的。
    普通人不知奥秘，只以为仙家法术是无所不能的，殊不知忌讳也多。
    很快，凌风子手中的葫芦就满了三大坛酒，然后付了酒钱，冷冷的瞥了一眼几日接着便大步离开了。
    当他刚刚走出茶肆的时候，却忽的看见茶肆外有一匹洁白的骏马立在那里，鼻息喷吐，声似蛟龙，浑身似乎冒着洁白的光芒，神异非凡。
    “这是.....龙驹？”凌风子看了一眼，惊的手中的酒葫芦都差点落在地上。
    天下竟有人以龙驹为坐骑？
    蓦地，他想起来了，这龙驹立于茶肆前，岂不是说茶肆之中有一人是龙驹之主？
    自己之前进来的时候居然没有留意。
    “那三人当中必有一人身负天命啊。”凌风子想到道藏之中的一句谶言。
    骑龙马，麒麟者，必负天命。
    心中一颤，不敢久留，急步离开。
    “唉，这凌风子道长可是不折不扣的高人啊，如今我等恶了这位高人，明日怕是万难求得仙缘了。”成乐此刻唉声叹气，一脸悲伤的说道。
    周才道：“若求不得仙缘不如回文县去吧，你我皆是秀才，走到哪也不怕没有营生，在哪活不是活呢？”
    成乐苦笑着调侃道：“周兄是放不下家中的娇妻吧，若不是我拉着周兄一起来，周兄怕是还不愿意呢，罢了，罢了，不说这个了，店家，这茶不喝了，上一坛酒吧，今日我要一醉方休。”
    心中最后的追求成了梦幻泡影，他想要借酒浇愁。
    掌柜的却十分抱歉的道：“这位客官，茶肆的酒水都被之前的凌风子道长买去了，现在没了，长寿镇是个小地方，饮酒的人不多，故而酒水酿的少，保存不得当会酸掉，还请客官见谅。”
    “唉，罢了，罢了。”成乐长叹一声，无力的挥了挥手道。
    “成乐，你在桌上写什么？”周才问道。
    成乐以手指沾茶水，在桌上写字，却见他字迹工整，笔力不凡，书法一定不错。
    “我要在桌上写三个惨字。”
    “......”李修远。
    周才似乎略有所感，也是重重一叹。
    李修远却道：“男子汉大丈夫唉声叹气的像什么话，既想饮酒，我这有美酒一壶，当与君共饮。”
    说着，他从鬼王布袋之中取出了一个玉壶，几个玉杯。
    酒非寻常酒，是当初真武神君设宴款待自己时候送给自己的，自己一杯未饮，不过酒香四溢，是仙酿。
    这是李修远的珍藏。
    虽是珍藏，但酒早晚都要喝的，这两个人品性不错，请自己喝茶，自己回请一杯酒水又有何不可？
