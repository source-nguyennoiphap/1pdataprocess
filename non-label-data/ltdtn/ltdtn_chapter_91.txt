    此刻出现在衙门外的鬼神并不少。
    足足有近千位。
    这样数量的鬼神自从鬼工头被灭杀之后便再也没有看到了。
    而且领头的鬼神长相怪异，虽然有着一副高大魁梧的身躯，但是头上顶着的却不是人的脑袋，而是一只牛的脑袋，还有马的脑袋，这样怪异的搭配在昏暗的夜晚里显得尤为可怕。
    这是马东和牛二。
    他们现在早已经不是寻常的鬼神一流了，而是已经成为了拥有千年道行的鬼王。
    放在阴间足以坐镇一座鬼城，统御千万厉鬼的存在。
    但在这里，他们却只是缉拿冤魂，捕捉恶鬼的不入流小神而已。
    “京城之中真需要你们的时候你们就来了，到是给了我一个意外惊醒啊。”很快，衙门之中一位身穿朱红色的官服的年轻男子面带笑容的大步走了过来。
    他一走来，衙门前所有的鬼神忍不住然后退了一步。
    却感觉那官服之上冒出红光，逼迫鬼神不敢靠近，又有一股让人鬼神畏惧的刚正念头涌来，鬼神碰到了就像是碰到了滚油一样，鬼躯都生疼万分，真不敢想象一旦靠近之后会发生什么事情。
    “拜见大少爷，让大少爷久等了，小的二人幸不辱命，完成了大少爷的嘱托。”马东和牛二急忙抱拳施礼。
    麾下近千鬼神齐齐单膝跪下：见过圣人。
    “无需多礼，”李修远虚扶了一下，然后笑道：“上我让你们带着过去镜抓一个妖人，你们都快抓半个月了，时间的确是有些久。”
    “还请大少爷恕罪，那妖人会一些奇异的法术，不好抓，而且抓捕的时候还遇到了一些妖物的阻扰，不得已才耽误了时间，还请大少爷恕罪。”马东颇为惭愧的说道。
    李修远挥了挥手道；“算了，人抓来了么？”
    “抓来了，还请大少爷过目。”马东挥了挥手，立刻就有两个阴兵绑着一只鬼魂送了过来。
    李修远看了一眼，辨认了一下，确定无误，的确是那个将卖羊的小贩，此刻他脸色惨白，浑身害怕的颤抖，显得很是恐惧，看样子之前这家伙没少吃苦头。
    “这人死了么？”
    “回大少爷话，还没死，小的只是见给他的魂魄勾来了。”一旁的牛二道；“大少爷上回送小的那勾魂锁很好使，这厮会一点法术也禁不住一勾。”
    “审问清楚了么？那些变成羊的女子都卖给了什么地方。”李修远道。
    马东沉声道：“小的审问了一番，此人说卖给了五通教。”
    “五通教？”李修远皱了皱眉：“具体卖给了哪个人，哪处地方？”
    “五通教掌柜的如意坊，有一位叫黄掌柜的人负责接手。”马东道。
    “有名有姓，有地点就好办，难怪京城的人都骂五通教妖人，底子到底还是不干净啊。”李修远道；“此事暂且放一旁，过去镜在么？我现在有急用。”
    “在的。”马东急忙取出了一面古镜，送了回去。
    李修远取来之后立刻就拿它查看石虎的位置。
    别让自己找到，找到之后今夜这厮必死无疑，断然不会让他活下去。
    顺着燕赤霞的那线索查找，翻看燕赤霞今日发生的种种事情。
    很快，在一个时辰之前燕赤霞在京城某处道君庙前找到了石虎，画面之中的石虎并没有想要躲躲藏藏的姿态，反而大大方方的在街道上走着。
    在燕赤霞发现石虎之后两人当街就打了起来，先是燕赤霞落了下风，随后便是夏侯武听到打斗声赶来支援，接着巡查京城的左千户也加入了战斗......四人在街道上交手，打的十分激烈。
    最后李修远看见夏侯武突然一剑刺穿了石虎的胳膊，而石虎也用刀柄重重的敲击了一下夏侯武的胸膛，接着负伤就逃。
    “你能逃到哪去？我过去镜能看过去种种，除非不漏踪迹，一旦漏了踪迹我就能锁定你，再也逃不出我的视线。”李修远目光微冷。
    石虎血很快止住了，这是用劲气强行堵住了伤口，武道宗师都会的手段。
    然后掩盖血腥味他偷到了运河一条商船之上。
    但没有躲多久，换了身衣服之后石虎就出来了，然后又往城东的方向走去了，是的，走去的，一点都不惊慌，丝毫没有担心自己会被再次发现。
    这头猛虎当真是胆大心细啊。
    “如意坊......”李修远最后看见这石虎大摇大摆的走进了如意坊里。
    接着过去镜便再也看不到了。
    过去镜不能查看屋内的情况，这是唯一缺点，这和玄光术不一样，玄光术可以。
    但玄光术会被懂法术的人感知，而且缺陷也大，容易被破和遮蔽。
    “石虎不是去如意坊快活吧？”李修远喃喃道；“不，不是，他的行动都是有计划的，先是去了道君庙，然后去了如意坊，那里是五通教的地盘，之前他还出现在皇宫和国师有交集。”
    想到这里。
    他目光变的凌厉起来：“等等，这厮在游说各方鬼神精怪。”
    瞬间，李修远便意识到了这石虎的意图，也明白了为什么这段时间雷神巡视京城的情况之下，这家伙还要暴露出来。
    国师，道君，五通教，加上这黑山老妖.......这是一股何其庞大的势力啊。
    要不是之前斩了鬼工头，倘若再算上那只水鬼的话，李修远感觉自己已经算是躺在了砧板上了，可以任由石虎动刀宰割了，便是有东岳神君的神权也斗不过这一个庞大的势力啊。
    “他今日才去了如意坊，说明算计还没有达成。”李修远微微眯着眼睛，收起了过去镜：“人算不如天算，这石虎怎么也没有想到我还有一手过去镜吧，否则以这厮的本事，一旦隐藏起来，鬼神都难寻觅，
    更别说诛杀他了。”
    “来人。”忽的他大声一喝。
    “大少爷有何吩咐？”衙门口的两个镖师急忙迎了上来。
    “把李超，吴象，崔魏唤来，再去派人通知夜叉将军，说我李某人有事相求，看他愿不愿意帮忙，若是愿意的话还请这位夜叉将军领精锐前来相助，就说事关五通教妖人。”
    “是，大人。”
    两人闻言，急忙抱拳施礼，然后离开传令去了。
    “大人要办案，怎么能少的了卑职，还请容卑职一同前去。”左千户这个时候走了出来施了一礼道。
    “左千户，你之前和石虎交手已经受了伤，这次我不想你去是担心你的安全。”李修远道：“此事并没有你想的那么简单，你若想帮忙的话，可以带着你的人手去如意坊附近候着，见机行事。”
    “卑职明白。”左千户立刻道。
    李修远又压着声音道：“在我没有到如意坊之前你决不能去城南，免得打草惊蛇。”
    如意坊如果有五通教人的话，必定有人能够未卜先知，察觉危险。
    他能算出左千户，却不能算出自己，所以这事情必须自己亲自去，只有如此才能蒙蔽天机，打他们一个措手不及。
    “大人放心，卑职一定按大人吩咐办事。”左千户点头道。
    他是武人，虽然李修远的命令虽然心中有疑惑，但绝对不会去质疑，只会去照办，这是他的一个优点，也是缺点。
    李修远吩咐下去之后，不禁负手而立，看着雷声滚滚的苍穹。
    今夜该自己上场了。
    可是就在这个时候，马东牛二带来的那群鬼神之中却传来了呼救声：“李大人救我，李大人救救妾身。”
    “嗯？马东，是哪个鬼魂在向我呼救？”李修远皱了皱眉。
    马东道；“回大少爷，应当是回来路上顺手抓拿的一些冤魂厉鬼吧，他们准备送往阴间的。”
    “这声音有些熟悉，带那鬼魂过来看看。”李修远挥了挥手道。
    调兵遣将，不急于这一时。
    “是，大少爷。”马东抱拳领命。
