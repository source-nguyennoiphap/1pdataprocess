import re
from os import listdir

# Lấy cả văn bản
def get_all_document(document_url):
    inF = open(document_url, "r")

    return inF.read()

# Lấy văn bản thành list với mỗi thành phần của list là một dòng
def get_document_by_line(url, file_name):
    list_lines = list()
    inF = open(url + file_name, "r")

    for line in inF:
        line = clean_chinese_data(line)
        if line != "":
            list_lines.append(line)

    inF.close()

    return list_lines

# Bỏ đi những phần thừa và kí tự không phù hợp trong văn bản
def clean_chinese_data(document):
    document = document.strip("\n")
    document = document.strip(" ")
    document = document.replace(u'\xa0', u' ')
    
    return document

# Lấy name từ file data
def get_listname(url):
    file_name = open(url, "r")
    chinese_name = list()

    for line in file_name:
        elements = line.split("=")
        if elements[0] != "":
            chinese_name.append(elements[0])

    return chinese_name

# Lấy mọi cái tên từ một câu bằng một danh sách các tên
def find_names(sentence, names):
    name_in_sentence = dict()
    
    for name in names:
        if name in sentence:
            list_start_indexs = [m.start() for m in re.finditer(name, sentence)]
            for index in list_start_indexs:
                name_in_sentence[name] = [index, index + len(name) - 1]

    return name_in_sentence

# Trả về một dictionary các từ với value là name hay không phải name
def label_word(document, list_name):
    list_name.sort(key=len, reverse=True)
    names_in_sentence = find_names(document, list_name)
    word_labeled = list()
    
    for word in document:
        word_labeled.append({'word':word, 'tag': 'O', 'used':False})

    for name in names_in_sentence:
        if name in document:
            start, end = names_in_sentence[name]
            if word_labeled[start]['used'] == False and word_labeled[end]['used'] == False:
                # word_not_using = [word[0] for word in enumerate(word_not_using) if word[0] >= start or word[0] <= end]
                
                # for index in range(start, end + 1):
                #     if index == start:
                #         word_labeled[document[start]] = "B"
                #     else:
                #         word_labeled[document[index]] = "I"
                for index in range(start, end + 1):
                    word_labeled[index]['used'] = True
                    if index == start:
                        word_labeled[index]['tag'] = 'B'
                    else:
                        word_labeled[index]['tag'] = 'I'
        else:
            print("something wrong")
    return word_labeled

# Ghi ra dữ liệu đã đánh nhãn ra file txt
def write_labeled_data(file_name, list_labeled_word):
    outF = open("data/labeled_document/phongvuyeu/" + file_name, "w")

    for chinese_word in list_labeled_word:
        outF.write(chinese_word['word'] + " " + chinese_word['tag'])
        outF.write("\n")
    outF.close()

if __name__ == "__main__":
    document_dir = "data/document/phongvuyeu/"
    pvy_names = get_listname("data/phongvuyeu/Names2.txt")
    general_names = get_listname("data/general/Names.txt")
    general_names.extend(pvy_names)
    document_files = listdir(document_dir)
    number_of_chapter = 0

    for document_file in document_files:
        number_of_chapter += 1
        list_sentences = get_document_by_line(document_dir, document_file)
        list_labeled_word = list()

        for sentence in list_sentences:
            list_labeled_word.extend(label_word(sentence, general_names))

        write_labeled_data(document_file, list_labeled_word)
        print("labeled success {} chapter".format(number_of_chapter))